FROM python:3.6-alpine3.7

RUN adduser -h /ros -D ros
COPY --chown=ros:ros . /ros/roscore
WORKDIR /ros/roscore

RUN apk add libxslt mariadb-client-libs && \
    apk add --no-cache --virtual .build-dependencies build-base libffi-dev libxml2 libxslt-dev mariadb-dev && \
    pip install . && \
    pip install 'mysqlclient ~= 1.3.13' && \
    apk del .build-dependencies

USER ros