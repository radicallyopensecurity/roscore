# Ros

Ros is a python library developed by RadicallyOpenSecurity to support Rosbot, the chatbot we use for so many things!

## Install

To install ros, you should run:

```bash
pip install git+ssh://git@gitlab.com/radicallyopensecurity/roscore.git
```

## Configuration
Most usages require a configuration file to be set up. Example:
```yaml
database:
        url: 'sqlite:///ros/db/ros.db'
flask:
        DEBUG: true
        SECRET_KEY: SuperSecretKey

```
Note that any relative database url path here will be relative to the `roscore` directory; not to the location of the configuration file itself. Also note that when you're installing roscore for the first time, you're not pointing towards an existing database, only to the place where you would like the database to be placed.

By default the configuration file is in `roscore/ros/configuration/config.yml` and the database will be installed in `roscore/ros/db/ros.db`.

Multiple configuration files can be merged, and the resulting configuration must be valid according the
[schema](./resources/configuration_schema.yml).

### First-time set-up / installation
From the `roscore` directory, run `./bin/ros-install --configuration-path ros/configuration/config.yml`. This will install a new database in the configured location.


## Usage

### Upgrades
Database upgrades are performed using [Alembic](http://alembic.zzzcomputing.com). All operations can be performed as per
its official documentation, but they must be prepended with `ROS_CONFIGURATION_PATH=[path_to_config_file]`. Example: to check
the current database schema status, run `ROS_CONFIGURATION_PATH=ros/configuration/config.yml alembic current`.

## Local Setup

(for docker setup, see below)

The easiest way to get started working on the ROS apps is to use a sqlite database, and a configuration file structured like:

```yaml
database:
  url: sqlite:///ros/db/ros.db
flask:
  DEBUG: true
  SECRET_KEY: SuperSecretKey

gitlab:
  url: https://gitlabs.radicallyopensecurity.com
  token: my-token
  namespace: ros-infra
  user:
    name: my-user
    email: my-email@localhost

rocketchat:
  url: http://10.1.1.4:3000
  user: my-user
  password: my-password
```

With this file in place, you can run: `tox -evenv ros-install -- --configuration ros/configuration/config.yml`.

After running these install steps, you are ready to run one of the web apps! The administration app can be run as follows:

```bash
$ uwsgi --http-socket '127.0.0.1:5000' --manage-script-name --master --no-orphans --module "ros.http.administration.entry_point:app" -H ./.tox/python3.6 --pyargv '--configuration-path ros/configuration/config.yml'
```

If this should fail, try leaving out the home argument (`-H ./.tox/python3.6`) and activating the virtual environment manually first:
```bash
$ . .tox/python3.6/bin/activate
(python3.6) 
$ uwsgi --http-socket '127.0.0.1:5000' --manage-script-name --master --no-orphans --module "ros.http.administration.entry_point:app" --pyargv '--configuration-path ros/configuration/config.yml'
```

In the above example, the administration app is specified with the 'ros.http.administration:app' argument, and the configuration file is, once again, the `config.yml` we created above.


## Docker setup

After installation, you can alternatively run the apps from a docker container. To do so, you'll first have to change some lines:

### Configure which app to run

In `Dockerfile.http`: 

Change the first line to 

```dockerfile
FROM roscore:latest
```

Set the app you'd like to run (in this example, the administration app) by changing `ENV APP=''` to:

```dockerfile
ENV APP='ros.http.administration.entry_point:app'
```

After this edit, build the `roscore` container:

```bash
$ docker build -t roscore .
```

build the `roscore.http` container using the `roscore` one you built earlier:

```bash
$ docker build -t roscore.http -f Dockerfile.http .
```

Then run the http container*:
```bash
$ docker run roscore.http
```

* On a Mac, you should probably use port forwarding; the app should then be reachable on https://localhost:5000
```bash
$ docker run roscore.http -p 5000:5000
```

If the above commands should result in failure to bind to port 5000, we've had success with either `docker run -p 5000:5000 -e APP=ros.http.administration.entry_point:app registry.gitlab.com/radicallyopensecurity/roscore/http:latest` (upstream) or `docker run -p 5000:5000 -e APP=ros.http.administration.entry_point:app roscore.http` (locally built)