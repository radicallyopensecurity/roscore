"""Added mailsystem tables

Revision ID: 446965d2635a
Revises: 
Create Date: 2019-10-13 09:52:44.129152

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '446965d2635a'
down_revision = None
branch_labels = None
depends_on = None


def downgrade():
    op.drop_index('ix_mailsystem-project_email', table_name='mailsystem-project')
    op.drop_index('ix_mailsystem-project_number', table_name='mailsystem-project')
    op.drop_table('mailsystem-project')
    op.drop_table('mailsystem-email')


def upgrade():
    op.create_table('mailsystem-project',
        sa.Column('id', sa.INTEGER(), nullable=False),
        sa.Column('name', sa.VARCHAR(length=255), nullable=False),
        sa.Column('short_name', sa.VARCHAR(length=255), nullable=False),
        sa.Column('number', sa.VARCHAR(length=20), nullable=True),
        sa.Column('applicant', sa.VARCHAR(length=255), nullable=True),
        sa.Column('email', sa.VARCHAR(length=255), nullable=True),
        sa.Column('type', sa.VARCHAR(length=9), nullable=True),
        sa.Column('welcome_email_sent', sa.BOOLEAN(), nullable=False),
        sa.Column('last_activity', sa.DATETIME(), nullable=False),
        sa.CheckConstraint("type IN ('pet', 'discovery')", name='projecttype'),
        sa.CheckConstraint('welcome_email_sent IN (0, 1)'),
        sa.PrimaryKeyConstraint('id')
    )
    op.create_table('mailsystem-email',
        sa.Column('id', sa.INTEGER(), nullable=False),
        sa.Column('server_id', sa.VARCHAR(length=255), nullable=True),
        sa.Column('sender_name', sa.VARCHAR(length=255), nullable=True),
        sa.Column('sender_address', sa.VARCHAR(length=255), nullable=True),
        sa.Column('subject', sa.VARCHAR(length=255), nullable=True),
        sa.Column('datetime', sa.DATETIME(), nullable=False),
        sa.Column('mailsystem-project_id', sa.INTEGER(), nullable=True),
        sa.ForeignKeyConstraint(['mailsystem-project_id'], ['mailsystem-project.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.create_index('ix_mailsystem-project_number', 'mailsystem-project', ['number'], unique=1)
    op.create_index('ix_mailsystem-project_email', 'mailsystem-project', ['email'], unique=False)
