# Copyright 2017 RadicallyOpenSecurity B.V.

import os

from setuptools import setup, find_packages


version_file = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                            'VERSION'))
with open(version_file) as v:
    VERSION = v.read().strip()


SETUP = {
    'name': "ros",
    'version': VERSION,
    'author': "ROS",
    # 'author_email': "",
    'url': 'https://gitlab.com/radicallyopensecurity/roscore/',
    'install_requires': [
        'bcrypt ~= 3.1.4',
        'basicauth ~= 0.4.1',
        'flask ~= 0.12.2',
        'flask-wtf ~= 0.14.2',
        'jsonschema ~= 2.6',
        'pyaml ~= 17.12.1',
        'requests ~= 2.18',
        'wtforms-alchemy ~= 0.16.6',
        'alembic ~= 0.9.7',
        'passlib ~= 1.7.1',
        'python-gitlab ~= 1.6.0',
        'rocketchat-API ~= 0.6.25',
        'sqlalchemy ~= 1.2.2',
        'sqlalchemy_enum_list ~= 0.1.1',
        'lxml ~= 4.2.5',
        'mysqlclient ~= 1.3.13',
        'flanker ~= 0.9.11',
    ],
    'packages': find_packages(),
    'data_files': ['ros/resources/configuration_schema.yml'],
    'include_package_data': True,
    'scripts': [
        'bin/ros-charge',
        'bin/ros-install',
        'bin/ros-migrate-1-to-2',
        'bin/ros-project-get',
        'bin/ros-upgrade',
        'bin/ros-user-get',
        'bin/mailsystem-get-mails',
    ],
    'license': "GPLv3",
    'long_description': open('README.md').read(),
    'description': 'ROS python helpers',
}

if __name__ == '__main__':
    setup(**SETUP)
