from jinja2 import Environment, PackageLoader, select_autoescape
from flask import url_for

import os


def render(name, sub='', *args, **kwargs):
    environment = Environment(
        loader=PackageLoader('ros', os.path.join('templates', sub)),
        autoescape=select_autoescape(['html', 'xml'])
    )
    template = environment.get_template('%s.html.j2' % name)
    return template.render(*args, **kwargs, url_for=url_for)


def render_page(title, content='', operations=(), empty_text=None, sub=''):
    return render('page', title=title, content=content, operations=operations, empty_text=empty_text, sub=sub)


def render_table(headers, rows):
    return render('table', headers=headers, rows=rows)


def render_link(title, url):
    return render('link', title=title, url=url)


def render_operations(operations):
    return render('operations', operations=operations)


def render_model_form(form, method, action, operations=(), require=True, random_password=False, **kwargs):
    return render('model_form',
                  form=form,
                  method=method,
                  action=action,
                  operations=operations,
                  require=require,
                  random_password=random_password,
                  **kwargs
                  )


def render_model_delete_form(form, method, action, operations=()):
    return render('model_delete_form', form=form, method=method, action=action, operations=operations)
