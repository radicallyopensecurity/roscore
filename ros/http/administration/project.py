from wtforms import SelectField

from ros.http.form import ModelDeleteForm, ModelForm
from ros.project import ProjectType, Project, PROJECT_TYPE_LABELS


class ProjectTypeField(SelectField):
    def __init__(self, **kwargs):
        SelectField.__init__(self, label='Type',
                             coerce=lambda x: x if isinstance(x, ProjectType) else ProjectType(x),
                             choices=list(PROJECT_TYPE_LABELS.items()), **kwargs)

    def iter_choices(self):
        for project_type, label in self.choices:
            selected = self.data is not None and project_type == self.data
            yield (project_type.value, label, selected)


class ProjectForm(ModelForm):
    class Meta:
        model = Project
        only = []

    type = ProjectTypeField()


class ProjectAddForm(ProjectForm):
    class Meta:
        only = ['type', 'name', 'rocketchat_room_name', 'rocketchat_id', 'gitlab_repository_name', 'gitlab_id']


class ProjectUpdateForm(ProjectForm):
    class Meta:
        only = ['name', 'rocketchat_room_name', 'rocketchat_id', 'gitlab_repository_name', 'gitlab_id']


class ProjectDeleteForm(ModelDeleteForm):
    class Meta:
        model = Project
        only = []
