from ros.auth import User, Role
from ros.http.form import CheckboxMultipleField, ModelDeleteForm, ModelForm


class RoleField(CheckboxMultipleField):
    def __init__(self, **kwargs):
        CheckboxMultipleField.__init__(self, label='Roles', coerce=lambda x: x if isinstance(x, Role) else Role(int(x)),
                                       choices={
                                           (Role.STAFF, 'Staff'),
                                           (Role.CUSTOMER, 'Customer'),
                                           (Role.PENTESTER, 'Pentester'),
                                           (Role.WRITER, 'Writer'),
                                           (Role.SYSOP, 'Sysop'),
                                           (Role.PM, 'Project manager'),
                                           (Role.MEMBER_NEW_OFFERTES, 'New offertes (what is this used for?)'),
                                           (Role.MEMBER_NEW_PENTESTS, 'New pentests (what is this used for?)'),
                                           (Role.VOLUNTEER, 'Volunteer'),
                                       }, **kwargs)

    def iter_choices(self):
        for role, label in self.choices:
            selected = self.data is not None and role in self.data
            yield (role.value, label, selected)


class UserForm(ModelForm):
    class Meta:
        model = User
        only = []

    roles = RoleField()


class UserAddForm(UserForm):
    class Meta:
        only = ['name', 'email', 'password', 'display_name', 'roles', 'expire']


class UserUpdateForm(UserForm):
    class Meta:
        only = ['name', 'email', 'password', 'display_name', 'roles', 'expire', 'rocketchat_id', 'gitlab_id']

    def validate(self):
        if self['password'].data == '' or self['password'].data is None:
            del self['password']
        return UserForm.validate(self)


class UserDeleteForm(ModelDeleteForm):
    class Meta:
        model = User
        only = []
