from flask import Flask, redirect, url_for
from werkzeug.exceptions import NotFound
import logging

from ros.auth import User
from ros.config import validate
from ros.html.render import render_page, render, render_link, render_operations, render_model_form, \
    render_model_delete_form
from ros.http.administration.project import ProjectAddForm, ProjectUpdateForm, ProjectDeleteForm
from ros.http.administration.user import UserUpdateForm, UserDeleteForm, UserAddForm
from ros.db import Database
from ros.project import Project, ProjectType, PROJECT_TYPE_LABELS


class App(Flask):
    def __init__(self, configuration):
        Flask.__init__(self, 'ros.http')
        self.logger_name = 'ros'
        validate(configuration)
        self._configuration = configuration
        self.config.update(configuration['flask'])
        self._register_routes()

    @property
    def db(self):
        return Database(self._configuration['database']['url'])

    def _register_routes(self):
        @self.route('/')
        def front():
            return render_page('Welcome to Radically Open Security')

        # @self.route('/static/<string:path>')
        # def static(path):
        #     return send_from_directory('static', path)

        @self.route('/user', methods=['GET'])
        def user_list():
            with self.db as db:
                headers = ('Username', 'Real name', 'Email address', 'Operations')
                rows = []
                for user in db.session.query(User):
                    operations = []
                    operations.append((url_for('user_update', id=user.id), 'Update'))
                    operations.append((url_for('user_delete', id=user.id), 'Delete'))
                    row = (
                        user.name, user.display_name, user.email, render_operations(operations))
                    rows.append(row)

                operations = [
                    (url_for('user_add'), 'Add a new user'),
                ]
                empty_text = 'There are no users yet. %s.' % render_link('Add a new user', url_for('user_add'))
                return render_page('Users', render('table', headers=headers, rows=rows, empty_text=empty_text), operations=operations)

        @self.route('/user/add', methods=['GET', 'POST'])
        def user_add():
            with self.db as db:
                user = User('', '', '')
                form = UserAddForm(obj=user)
                if form.validate_on_submit():
                    form.populate_obj(user)
                    db.session.add(user)
                    user.initial_setup(self._configuration)
                    db.session.flush()
                    db.session.commit()
                    return redirect('/user')

            return render_page('Add a new user', render_model_form(form, 'POST', '/user/add', random_password=True))

        @self.route('/user/<int:id>/update', methods=['GET', 'POST'])
        def user_update(id):
            with self.db as db:
                user = db.session.query(User).filter(User.id == id).first()

                if user is None:
                    return NotFound()

                form = UserUpdateForm(obj=user)
                if form.validate_on_submit():
                    form.populate_obj(user)
                    logging.info("About to update user:\n{}".format(user))
                    db.session.add(user)
                    db.session.flush()
                    db.session.commit()
                    return redirect('/user/{}/update'.format(id))

            operations = [
                (url_for('user_delete', id=user.id), 'Delete'),
            ]
            return render_page('Update user %s' % user.name,
                               render_model_form(form, 'POST', '/user/%d/update' % user.id, operations,
                                                 require=False,
                                                 random_password=True,
                                                 user=user))

        @self.route('/user/<int:id>/disable', methods=['POST'])
        def user_disable(id):
            with self.db as db:
                user = db.session.query(User).filter(User.id == id).first()

                if user is None:
                    return NotFound()
                user.disable(self._configuration)
                db.session.flush()
                db.session.commit()
            return redirect('/user')

        @self.route('/user/<int:id>/enable', methods=['POST'])
        def user_enable(id):
            with self.db as db:
                user = db.session.query(User).filter(User.id == id).first()

                if user is None:
                    return NotFound()
                user.enable(self._configuration)
                db.session.flush()
                db.session.commit()
            return redirect('/user')

        @self.route('/user/<int:id>/delete', methods=['GET', 'POST'])
        def user_delete(id):
            with self.db as db:
                user = db.session.query(User).filter(User.id == id).first()

                if user is None:
                    return NotFound()

                form = UserDeleteForm(obj=user)

                if form.validate_on_submit():
                    db.session.delete(user)
                    db.session.flush()
                    db.session.commit()
                    return redirect('/user')

                operations = [
                    (url_for('user_update', id=user.id), 'Cancel'),
                ]
                return render_page('Delete user %s' % user.name,
                                   render_model_delete_form(form, 'POST', '/user/%d/delete' % user.id, operations))

        @self.route('/project', methods=['GET'])
        def project_list():
            with self.db as db:
                headers = ('Name', 'Type', 'Operations')
                rows = []
                for project in db.session.query(Project):
                    operations = []
                    operations.append((url_for('project_view', project_id=project.id), 'View'))
                    operations.append((url_for('project_update', project_id=project.id), 'Update'))
                    operations.append((url_for('project_delete', project_id=project.id), 'Delete'))
                    row = (
                        project.name, PROJECT_TYPE_LABELS[project.type], render_operations(operations))
                    rows.append(row)

                operations = [
                    (url_for('project_add'), 'Add a new project'),
                ]
                empty_text = 'There are no projects yet. %s.' % render_link('Add a new project', url_for('project_add'))
                return render_page('Projects', render('table', headers=headers, rows=rows, empty_text=empty_text), operations=operations)

        @self.route('/project/<int:project_id>', methods=['GET'])
        def project_view(project_id):
            with self.db as db:
                project = db.session.query(Project).filter(Project.id == project_id).first()

                if project is None:
                    return NotFound()

            operations = [
                (url_for('project_update', project_id=project.id), 'Update'),
                (url_for('project_delete', project_id=project.id), 'Delete'),
            ]
            if project.chat_url:
                operations.append((project.chat_url, 'Chat'))
            if project.repository_url:
                operations.append((project.repository_url, 'Repository'))
            return render_page(project.name, '', operations)

        @self.route('/project/add', methods=['GET', 'POST'])
        def project_add():
            with self.db as db:
                project = Project(ProjectType.QUOTE, '')
                form = ProjectAddForm(obj=project)

                if form.validate_on_submit():
                    form.populate_obj(project)
                    db.session.add(project)
                    db.session.flush()
                    db.session.commit()
                    return redirect('/project')

            return render_page('Add a new project', render_model_form(form, 'POST', '/project/add'))

        @self.route('/project/<int:project_id>/update', methods=['GET', 'POST'])
        def project_update(project_id):
            with self.db as db:
                project = db.session.query(Project).filter(Project.id == project_id).first()

                if project is None:
                    return NotFound()

                form = ProjectUpdateForm(obj=project)

                if form.validate_on_submit():
                    form.populate_obj(project)
                    db.session.add(project)
                    db.session.flush()
                    db.session.commit()
                    return redirect('/project')

            operations = [
                (url_for('project_delete', project_id=project.id), 'Delete'),
            ]
            return render_page('Update project %s' % project.name,
                               render_model_form(form, 'POST', '/project/%d/update' % project.id, operations))

        @self.route('/project/<int:project_id>/delete', methods=['GET', 'POST'])
        def project_delete(project_id):
            with self.db as db:
                project = db.session.query(Project).filter(Project.id == project_id).first()

                if project is None:
                    return NotFound()

                form = ProjectDeleteForm(obj=project)

                if form.validate_on_submit():
                    db.session.delete(project)
                    db.session.flush()
                    db.session.commit()
                    return redirect('/project')

                operations = [
                    (url_for('project_update', project_id=project.id), 'Cancel'),
                ]
                return render_page('Delete project %s' % project.name,
                                   render_model_delete_form(form, 'POST', '/project/%d/delete' % project.id, operations))
