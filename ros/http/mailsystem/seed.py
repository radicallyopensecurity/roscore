from app import db
from app.models import Project, ProjectType

import json
import os


for filename in os.listdir('projects'):
    with open('projects/' + filename, 'r') as f:

        if os.environ.get("FLASK_ENV") == 'development':
            if "TEST" not in filename:
                continue
        else:
            if "TEST" in filename:
                continue

        print(filename)

        projects = json.load(f)

        if filename.endswith('P.json'):
            type = ProjectType.pet
        elif filename.endswith('D.json'):
            type = ProjectType.discovery
        else:
            continue

    for project in projects:
        print(project['id'])
        p = Project(
            name=str(project['name']),
            short_name=str(project['shortname']),
            number=str(project['id']),
            applicant=str(project['applicant']),
            email=str(project['mail']),
            type=type)
        db.session.add(p)

db.session.commit()
