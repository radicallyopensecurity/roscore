import argparse

from ros.cli.args import add_configuration_to_args, parse_configuration_from_args
from ros.http.mailsystem.app import App

# This script provides a WSGI entry point. Some script arguments are needed. Pass '--help' as the first argument for
# documentation.
parser = argparse.ArgumentParser()
add_configuration_to_args(parser)
cli_args = parser.parse_args()
configuration = parse_configuration_from_args(cli_args)
app = App(configuration)
