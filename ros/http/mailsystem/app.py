from flask import Flask, redirect, request

from ros.config import validate
from ros.html.render import render_page, render_model_form, render
from ros.helpers import send_email
from ros.mailsystem import Project, ProjectType, Email
from ros.db import Database
from ros.http.mailsystem.project import ProjectsAddForm

from datetime import datetime

import json


class App(Flask):
    def __init__(self, configuration):
        Flask.__init__(self, 'ros.http')
        self.logger_name = 'ros'
        validate(configuration)
        self._configuration = configuration
        self.config.update(configuration['flask'])
        self._register_routes()

    @property
    def db(self):
        return Database(self._configuration['database']['url'])

    def _register_routes(self):
        @self.route('/')
        def front():
            return render_page('Welcome to the Mailsystem', sub='mailsystem')

        @self.route('/emails', methods=['GET', 'POST'])
        def emails():

            messages = []

            with self.db as db:
                if request.method == 'POST':
                    email_id = request.form.get('email')
                    project_id = request.form.get('project')

                    project = db.session.query(Project).get(project_id)
                    email = db.session.query(Email).get(email_id)
                    email.project_id = project.id
                    db.session.commit()

                    messages.append('Successfully added an email to project ' + project.name)

                new_emails = db.session.query(Email).filter_by(_mailsystem_project_id=None)
                attached_emails = db.session.query(Email).filter(Email._mailsystem_project_id is not None).all()

                return render_page('Emails', render('emails', title='emails', messages=messages,
                                   new_emails=new_emails, attached_emails=attached_emails, sub='mailsystem'),
                                   sub='mailsystem', operations=[])

        @self.route('/emails/<email_id>')
        def email_details(email_id):

            with self.db as db:
                email = db.session.query(Email).get(email_id)

                return email.from_name

        @self.route('/projects')
        def projects(**kwargs):
            with self.db as db:
                projects = db.session.query(Project).all()

                return render_page(
                    'Projects',
                    render('projects', **kwargs, title='projects', projects=projects, sub='mailsystem'),
                    sub='mailsystem',
                    operations=[])

        @self.route('/projects/<id>')
        def project(id):
            with self.db as db:
                project = db.session.query(Project).get(id)

                return render_page('Project', render('project', title='project', project=project, sub='mailsystem'), sub='mailsystem', operations=[])

        @self.route('/projects/add', methods=['GET', 'POST'])
        def new_projects():

            with self.db as db:
                form = ProjectsAddForm()
                if form.validate_on_submit():
                    projects_array = json.loads(form.projects.data)
                    # TODO Following block should be parsed according to config (maybe with chain of resp pattern)
                    for temp_project in projects_array["fund"]["proposals"]:
                        print(temp_project["id"])
                        project = Project(
                            name=temp_project["proposal"]["projectName"],
                            short_name=temp_project["proposal"]["projectName"],
                            number=temp_project["id"],
                            applicant=temp_project["proposal"]["contact"]["name"],
                            email=temp_project["proposal"]["contact"]["email"],
                            type=ProjectType.pet if temp_project["proposal"]["fund"] == "PET_Fund" else ProjectType.discovery,
                            welcome_email_sent=False,
                            last_activity=datetime.now())
                        db.session.add(project)

                    db.session.flush()
                    db.session.commit()
                    return redirect('/projects')

            return render_page('Add mailsystem projects', render_model_form(form, 'POST', '/projects/add'), sub='mailsystem')

        @self.route('/send-welcome-email', methods=['POST'])
        def send_welcome_email():
            messages = []
            with self.db as db:
                for p in request.form.getlist('project'):
                    project = db.session.query(Project).get(p)

                    if project.welcome_email_sent:
                        messages.append("No welcome email sent to " + project.applicant + " for project " + project.name + " because it was already sent!")
                    else:
                        send_email(
                            from_name='Radically Open Security',
                            from_email=self._configuration['mailsystem']['email_user'],
                            to_name=project.applicant,
                            to_email=project.email,
                            cc_name=project.call,
                            cc_email=project.call + '@' + self._configuration['mailsystem']['cc_domain'],
                            subject='Basic Security Quickscan for ' + project.number + ': ' + project.name,
                            body=open(self._configuration['mailsystem']['email_body_file_path']).read().replace('{{ name }}', project.applicant),
                            smtp_server=self._configuration['smtp-server'],
                        )

                        project.welcome_email_sent = True
                        db.session.commit()

                        messages.append("welcome email sent to " + project.applicant + " for project " + project.name)

                return projects(messages=messages)
