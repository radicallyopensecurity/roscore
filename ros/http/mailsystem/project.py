from ros.http.form import ModelForm
from wtforms import TextAreaField


class ProjectsForm(ModelForm):
    class Meta:
        only = []


class ProjectsAddForm(ProjectsForm):
    class Meta:
        only = ['projects']

    projects = TextAreaField()
