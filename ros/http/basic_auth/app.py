from basicauth import decode, DecodeError
from flask import Flask, request, Response

from ros.auth import Auth, User
from ros.config import validate
from ros.db import Database


class App(Flask):
    def __init__(self, configuration):
        Flask.__init__(self, 'ros.http')
        self.logger_name = 'ros'
        validate(configuration)
        self._configuration = configuration
        self.config.update(configuration['flask'])
        self.database = Database(self._configuration['database']['url'])
        self._register_routes()

    @property
    def db(self):
        return Database(self._configuration['database']['url'])

    def _register_routes(self):
        @self.route('/auth/basic/<permission>', methods=['GET'])
        def http_basic_auth(permission):
            if 'Authorization' not in request.headers:
                return self._deny_because_unauthenticated()

            try:
                username, password = decode(request.headers['Authorization'])
            except DecodeError:
                return self._deny_because_unauthenticated()

            with self.db as db:
                auth = Auth(db)
                user = auth.authenticate(username, password)
            if not isinstance(user, User):
                return self._deny_because_unauthenticated()
            db.session.refresh(user)
            if user.cannot(permission):
                return self._deny_because_unauthorized()

            return self._allow()

    def _deny_because_unauthenticated(self):
        return Response(None, 401)

    def _deny_because_unauthorized(self):
        return Response(None, 403)

    def _allow(self):
        return Response(None, 200)
