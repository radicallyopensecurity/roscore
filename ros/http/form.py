from flask import current_app
from flask_wtf import FlaskForm
from wtforms import SelectMultipleField
from wtforms.form import FormMeta
from wtforms.widgets import ListWidget, CheckboxInput
from wtforms_alchemy import model_form_factory, model_form_meta_factory

from ros.db import Database


class _ModelFormMeta(FormMeta):
    def __call__(cls, *args, **kwargs):
        # The `Meta.only` field is not properly applied by WTForms-SQLAlchemy, so we do that here:
        # 1) Call the parent form factory, which prepares the unbound form fields, and builds the form.
        # 2) Discard the returned form, because it contains excluded fields.
        # 3) Filter the unbound fields.
        # 4) Call the form factory again, which will now operate on the correct unbound fields, and return the form.
        FormMeta.__call__(cls, *args, **kwargs)
        if cls.Meta.only is not None:
            cls._unbound_fields = [(name, field) for (name, field) in cls._unbound_fields if name in cls.Meta.only]
        return FormMeta.__call__(cls, *args, **kwargs)


ModelFormMeta = model_form_meta_factory(_ModelFormMeta)


class _ModelForm(FlaskForm):
    class Meta:
        strip_string_fields = True

    @classmethod
    def get_session(self):
        # @todo How do we close this session's connection?
        return Database(current_app.configuration['database']['url']).session


ModelForm = model_form_factory(_ModelForm, meta=ModelFormMeta)


class ModelDeleteForm(ModelForm):
    class Meta:
        all_fields_optional = True


class CheckboxMultipleField(SelectMultipleField):
    widget = ListWidget(prefix_label=False)
    option_widget = CheckboxInput()
