from tempfile import NamedTemporaryFile

from sqlalchemy import (
    create_engine,
)
import sqlalchemy.pool as pool
from sqlalchemy.orm import sessionmaker

from ros.model import metadata, import_models


class Database:
    """Database is a wrapper on the SQL query facilities to allow for better encapsulation"""

    def __init__(self, url):
        self._engine = create_engine(
            url,
            pool_size=20,
            max_overflow=0,
            pool_pre_ping=True,
            pool_recycle=10,
            poolclass=pool.QueuePool,
            echo_pool=True)
        self._connection = None
        self._session = None

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.close()

    def close(self):
        if self._connection:
            self._connection.close()

    @property
    def engine(self):
        return self._engine

    @property
    def connection(self):
        if self._connection is None:
            self._connection = self._engine.connect()
        return self._connection

    @property
    def session(self):
        """
        Gets an ORM session for querying the database.
        :return:
        """
        if self._session is None:
            self._session = sessionmaker(self._engine)()
        return self._session


class TemporaryDatabase(Database):
    """Provides a temporary, SQLite-backed database."""

    def __init__(self):
        self._f = NamedTemporaryFile()
        self._db_url = 'sqlite:///%s' % self._f.name
        Database.__init__(self, self._db_url)
        install(self)

    def __exit__(self, *args):
        self.close()

    def close(self):
        self._f.close()
        Database.close(self)

    @property
    def url(self):
        return self._db_url


def install(db):
    import_models()
    metadata.bind = db.engine
    metadata.create_all(db.engine)


def upgrade(db):
    raise NotImplementedError()
