import re
import smtplib
from email.mime.application import MIMEApplication
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart


def locale_to_float(input):
    """
    Parse a numeric string to a float.
    """
    if not re.fullmatch(r'^\d+(\.\d+)?$', input):
        raise ValueError('Numbers must me formatted as digits, with an optional period as decimal separator.')
    return float(input)


def render_template(template, opts={}):
    """
    Render a template, replacing %placeholder% with the value
    `placeholder` from the opts dict
    """
    for key, value in opts.items():
        template = template.replace('%{}%'.format(key), value)
    return template


def send_email(from_name, from_email, to_name, to_email, subject, body, cc_name=None, cc_email=None, attachments={}, smtp_server="mail"):
    msg = MIMEMultipart()
    msg['Subject'] = subject
    msg['From'] = "{} <{}>".format(from_name, from_email)
    msg['To'] = "{} <{}>".format(to_name, to_email)
    if cc_name is not None and cc_email is not None:
        msg['Cc'] = "{} <{}>".format(cc_name, cc_email)

    msg.attach(MIMEText(body))
    for name, contents in attachments.items():
        part = MIMEApplication(
            contents,
            Name=name,
        )
        part.add_header('Content-Disposition', 'attachment', filename=name)
        msg.attach(part)
    with smtplib.SMTP(smtp_server) as server:
        server.send_message(msg)
        # server.sendmail(from_addr, to_addr, body)
