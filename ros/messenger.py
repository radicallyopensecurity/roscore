# coding=utf-8

import sys

import requests


class Messenger:
    def state(self, message):
        pass

    def inform(self, message):
        pass

    def confirm(self, message):
        pass

    def alert(self, message):
        pass


class GroupedMessengers(Messenger):
    def __init__(self):
        self._messengers = []

    def add_messenger(self, messenger):
        self._messengers.append(messenger)

    def state(self, message):
        for messenger in self._messengers:
            messenger.state(message)

    def inform(self, message):
        for messenger in self._messengers:
            messenger.inform(message)

    def confirm(self, message):
        for messenger in self._messengers:
            messenger.confirm(message)

    def alert(self, message):
        for messenger in self._messengers:
            messenger.alert(message)


class StdioMessenger(Messenger):
    def _print(self, message, color):
        print('\033[0;%dm  \033[0;1;%dm %s\033[0m' % (color + 40, color + 30, message), file=sys.stderr)

    def state(self, message):
        self._print(message, 7)

    def inform(self, message):
        self._print(message, 6)

    def confirm(self, message):
        self._print(message, 2)

    def alert(self, message):
        self._print(message, 1)


class RosbotMessenger(Messenger):
    def __init__(self, address, room_name):
        self._url = '{}/hubot/automessage/{}'.format(address, room_name)

    def _message(self, message, prefix):
        requests.post(self._url, json={
            'msg': '%s %s' % (prefix, message),
        })

    def state(self, message):
        self._message(message, '🤖')

    def inform(self, message):
        self._message(message, 'ℹ')

    def confirm(self, message):
        self._message(message, '✔')

    def alert(self, message):
        self._message(message, '❌')
