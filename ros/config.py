"""Provides configuration components."""
import json
import logging
import os
from logging import config as logging_config
from tempfile import NamedTemporaryFile

import yaml
from jsonschema import validate as json_validate, ValidationError

from ros.messenger import GroupedMessengers

FORMAT_JSON_EXTENSIONS = ('json',)
FORMAT_YAML_EXTENSIONS = ('yml', 'yaml')


class ConfigurationError(ValueError):
    """Indicate an error with the application configuration."""

    pass


class ConfigurationFileError(ValueError):
    """Indicate an error with the application configuration file."""

    pass


class ConfigurationValueError(ValueError):
    """Indicate an error with the application configuration values."""

    pass


def validate(configuration):
    """Validates configuration.

    :param configuration: Dict
    :raise: ConfigurationValueError
    """
    with open(os.path.join(os.path.dirname(__file__), 'resources', 'configuration_schema.yml')) as f:
        schema = yaml.safe_load(f)
    try:
        json_validate(configuration, schema)
    except ValidationError as e:
        raise ConfigurationValueError((str(e)))


def from_file(f):
    """Parse configuration from a file.

    :param f: File
    :return: Dict
    :raise: ConfigurationError
    """
    if any(map(f.name.endswith, FORMAT_JSON_EXTENSIONS)):
        configuration = json.load(f)
    elif any(map(f.name.endswith, FORMAT_YAML_EXTENSIONS)):
        configuration = yaml.safe_load(f)
    else:
        raise ConfigurationFileError(
            'Configuration files must have *.json, *.yml, or *.yaml extensions.')
    validate(configuration)
    configuration['smtp-server'] = configuration.get('smtp-server', 'mail')
    return configuration


class Bootstrapped:
    """
    Enters a context in which the configuration is bootstrapped.

    Exceptions are caught, logged, and communicated to the user.
    """

    def __init__(self, configuration, messenger=None):
        """Initialize the context.

        :param configuration: dict
        :param messenger: Optional[ros.messenger.Messenger]
        """
        self._configuration = configuration
        self._messenger = GroupedMessengers()
        if messenger:
            self._messenger.add_messenger(messenger)

    def __enter__(self):
        validate(self._configuration)

        if 'logging' in self._configuration:
            logging_config.dictConfig(self._configuration['logging'])

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type is None:
            return False

        if issubclass(exc_type, KeyboardInterrupt):
            self.messenger.alert('Execution was interrupted.')
            return True

        self.logger.exception('A fatal error occurred.')
        self.messenger.alert('A fatal error occurred. Details have been logged.')
        return True

    @property
    def messenger(self):
        return self._messenger

    @property
    def logger(self):
        return logging.getLogger('ros')


class TemporaryConfiguration:
    """Provides a temporary configuration file."""

    def __init__(self, configuration):
        validate(configuration)
        self._f = NamedTemporaryFile(mode='w+t', suffix='.json')
        json.dump(configuration, self._f)
        self._f.seek(0)

    def __enter__(self):
        return self._f

    def __exit__(self, *args):
        self.close()

    def close(self):
        self._f.close()
