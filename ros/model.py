from sqlalchemy import MetaData
from sqlalchemy.ext.declarative import declarative_base

metadata = MetaData()
Base = declarative_base(metadata=metadata)


class Serializable:
    """Define a model that can be serialized."""

    def serialize(self):
        return {
            'id': self.id,
        }


def import_models():
    """Import all models, upon which they register themselves with SQLAlchemy automatically"""
    import ros.auth  # noqa F401
    import ros.project  # noqa F401
    import ros.cli.charge  # noqa F401
    import ros.mailsystem  # noqa F401
