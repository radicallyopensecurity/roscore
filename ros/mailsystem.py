from datetime import datetime

from sqlalchemy import Column, Integer, String, ForeignKey, DateTime, Enum, Boolean
# from sqlalchemy.orm import relationship
from sqlalchemy.ext.hybrid import hybrid_property

import enum

from ros.model import Base, Serializable
from ros.merge import merge


class ProjectType(enum.Enum):
    pet = "PET"
    discovery = "DISCOVERY"


class Project(Base, Serializable):
    __tablename__ = 'mailsystem-project'
    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False)
    short_name = Column(String(255), nullable=False)
    number = Column(String(20), index=True, unique=True)
    applicant = Column(String(255))
    email = Column(String(255), index=True)
    type = Column(Enum(ProjectType))
    welcome_email_sent = Column(Boolean, default=False, nullable=False)
    # correspondence = relationship('Email', backref='project', lazy=True)
    last_activity = Column(DateTime, nullable=False, default=datetime.utcnow)

    @hybrid_property
    def call(self):
        return self.number[0:7] + str(self.type)[0]

    def __init__(self, name, short_name, number, applicant, email, type, welcome_email_sent, last_activity):
        self.name = name
        self.short_name = short_name
        self.number = number
        self.applicant = applicant
        self.email = email
        self.type = type
        self.welcome_email_sent = welcome_email_sent
        self.last_activity = last_activity

    def serialize(self):
        return merge(Serializable.serialize(self), {
            'name': self.name,
            'short_name': self.short_name,
            'number': self.number,
            'applicant': self.applicant,
            'email': self.email,
            'type': self.type,
            'welcome_email_sent': self.welcome_email_sent,
            'last_activity': self.last_activity,
        })


class Email(Base, Serializable):
    __tablename__ = 'mailsystem-email'
    id = Column(Integer, primary_key=True)
    server_id = Column(String(255))
    sender_name = Column(String(255))
    sender_address = Column(String(255))
    subject = Column(String(255))
    datetime = Column(DateTime, nullable=False, default=datetime.utcnow)
    _mailsystem_project_id = Column('mailsystem-project_id', Integer, ForeignKey(Project.id), nullable=True)

    @property
    def possible_projects(self):
        return Project.query.filter_by(email=self.sender_address)

    def __init__(self, server_id, sender_name, sender_address, subject, datetime):
        self.server_id = server_id
        self.sender_name = sender_name
        self.sender_address = sender_address
        self.subject = subject
        self.datetime = datetime
