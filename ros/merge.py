"""Provides merge components."""
from copy import copy

from functools import singledispatch


@singledispatch
def merge(original, override):
    """Merges two values together, with the second overriding the first one.

    :param one: Any
    :param override: Any
    :return: Any
    """
    return override


@merge.register(dict)
def merge_dict(original, override):
    """Merges two dictionaries.

    :param original: Dict
    :param override: Dict
    :return: Dict
    """
    result = copy(original)
    for key, value in override.items():
        if key in result:
            value = merge(result[key], value)
        result[key] = value
    return result
