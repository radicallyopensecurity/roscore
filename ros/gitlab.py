import gitlab
from base64 import b64decode


def client(configuration):
    """
    Create a new GitLab client based on ROS configuration.

    :return: gitlab.Gitlab
    """
    return gitlab.Gitlab(configuration['gitlab']['url'], configuration['gitlab']['token'])


def get_file(client, repo, file, ref='master', decode=True):
    """
    Fetch a file from a gitlab instance and return the decoded
    content.

    :param client: a gitlab.Gitlab client
    :param repo: the path to the repo
    :param file: the path to the file within the repo
    :param ref: which ref to fetch from
    :param decode: Should the file's content be decoded into a str

    :returns: The contents of the file as a str"
    """
    project = client.projects.get(repo)
    try:
        file = project.files.get(file_path=file, ref=ref)
    except gitlab.exceptions.GitlabGetError:
        print("Encountered an error fetching {}/{}".format(repo, file))
        return None
    content = b64decode(file.content)
    if decode:
        print("About to try to decode {}".format(content))
        try:
            return content.decode('utf-8', 'ignore')
        except UnicodeDecodeError:
            print("Failed to decode utf-8")
            pass
        except Exception as e:
            print("Failed to decode ASCII: {}".format(e))
            return content
    return content
