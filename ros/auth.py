from datetime import datetime
from enum import Enum
from functools import wraps
import logging
import traceback

from sqlalchemy import Column, Integer, String, DateTime, or_
from sqlalchemy_enum_list import EnumSetType
from sqlalchemy_utils import PasswordType

from ros import gitlab
from ros import rocketchat
from ros.helpers import render_template, send_email
from ros.merge import merge
from ros.model import Base, Serializable


class Role(Enum):
    STAFF = 1
    CUSTOMER = 2
    PENTESTER = 3
    WRITER = 4
    SYSOP = 5
    PM = 6
    MEMBER_NEW_OFFERTES = 7
    MEMBER_NEW_PENTESTS = 8
    VOLUNTEER = 9


INTERNAL = [
    'use.chat',
    'use.gitlabs',
]
EXTERNAL = INTERNAL

PERMISSIONS = {
    Role.STAFF: INTERNAL + ['use.elk'],
    Role.CUSTOMER: EXTERNAL,
    Role.PENTESTER: INTERNAL,
    Role.WRITER: INTERNAL,
    Role.SYSOP: ['use.admin'],
    Role.PM: INTERNAL,
    Role.MEMBER_NEW_OFFERTES: [],
    Role.MEMBER_NEW_PENTESTS: [],
    Role.VOLUNTEER: [],
}

ROCKETCHAT_CHANNELS = ['ros', 'ros-onboarding', 'ros-offtopic', 'ros-assignments', 'ros-chatops']


class AuthError(RuntimeError):
    pass


class NotAuthenticatedError(AuthError):
    pass


class NotAuthorizedError(AuthError):
    pass


class User(Base, Serializable):
    __tablename__ = 'user'
    id = Column('user_id', Integer, primary_key=True, info={
        'label': 'ID',
    })
    name = Column('user_name', String(50), nullable=False, info={
        'label': 'Username',
    })
    password = Column('password', PasswordType(schemes=['bcrypt']), nullable=False, info={
        'label': 'Password',
    })
    display_name = Column('realname', String(32), info={
        'label': 'Real name',
    })
    email = Column('email', String(50), nullable=False, info={
        'label': 'Email address',
    })
    expire = Column('expire', DateTime, info={
        'label': 'Expiration date',
    })
    last_login = Column('last_login', DateTime, info={
        'label': 'Last log-in',
    })
    rocketchat_id = Column('rocketchat_id', String(50), info={
        'label': 'RocketChat user ID',
    })
    gitlab_id = Column('gitlab_id', Integer, info={
        'label': 'GitLab user ID',
    })
    roles = Column('roles', EnumSetType(Role, int), nullable=False, default=lambda: [], info={
        'label': 'Roles',
    })

    def __init__(self, name, password, email):
        self.name = name
        self.password = password
        self.email = email
        self.roles = []

    def serialize(self):
        return merge(Serializable.serialize(self), {
            'name': self.name,
            'email': self.email,
            'rocketchat-id': self.rocketchat_id,
            'gitlab-id': self.gitlab_id,
            'roles': list(role.name for role in self.roles or []),
        })

    def can(self, permission):
        """
        Checks if the user has the given permission.
        :param permission: str
        :return: bool
        """
        if self.expire is None or self.expire > datetime.now():
            for role in self.roles:
                if role in PERMISSIONS:
                    if permission in PERMISSIONS[role]:
                        return True
        return False

    def cannot(self, permission):
        """
        Checks if the user does not have the given permission.
        :param permission: str
        :return: bool
        """
        return not self.can(permission)

    def expired(self):
        return self.expire and self.expire < datetime.now()

    def initial_setup(self, configuration):
        self.ensure_gitlab_user(configuration)
        self.ensure_rocketchat_user(configuration)
        try:
            self.send_welcome_email(configuration)
        except Exception as e:
            logging.error("Failed to send welcome email: {}".format(e))
            logging.error(traceback.format_exc())
            print("Failed to send welcome email: {}".format(e))

    def send_welcome_email(self, configuration):
        """Send a welcome email to this user"""
        logging.info("Sending welcome email to {}".format(self.name))
        gitlab_client = gitlab.client(configuration)
        file = None
        if Role.CUSTOMER in self.roles:
            file = "Onboarding_customers/welcome-email.txt"
            onboarding_manual = "Onboarding_customers/onboarding_manual_full.pdf"
        else:
            file = "Onboarding_manuals/welcome-email.txt"
            onboarding_manual = "Onboarding_manuals/onboarding_manual_full.pdf"

        if file:
            template = gitlab.get_file(
                gitlab_client,
                'ros/General_Info',
                file,
            )
            if not template:
                return
            opts = {
                'username': self.name,
            }
            body = render_template(template, opts)
            attachments = {}
            manual = gitlab.get_file(
                gitlab_client,
                'ros/General_Info',
                onboarding_manual,
                decode=False
            )
            if manual:
                attachments["ROS-onboarding-manual.pdf"] = manual

            send_email(
                from_name='rosbot',
                from_email='rosbot@radicallyopensecurity.com',
                to_name=self.display_name,
                to_email=self.email,
                subject="Welcome to Radically Open Security",
                body=body,
                attachments=attachments,
                smtp_server=configuration.get('smtp-server'),
            )

    def ensure_gitlab_user(self, configuration):
        logging.info("Ensuring user {} has a gitlab account".format(self.name))
        gitlab_client = gitlab.client(configuration)
        if not gitlab_client.users.list(username=self.name):
            user_data = {
                'email': self.email,
                'username': self.name,
                'name': self.name,
                'skip_confirmation': True,
                'password': self.password.secret,
            }
            user = gitlab_client.users.create(user_data)
            self.gitlab_id = user.id
            if Role.STAFF in self.roles:
                repos = ['ros/General_Info']
                for repo in repos:
                    project = gitlab_client.projects.get(repo)
                    project.members.create({
                        'user_id': self.gitlab_id,
                        'access_level': gitlab.gitlab.DEVELOPER_ACCESS,
                    })

    def ensure_rocketchat_user(self, configuration):
        logging.info("Ensuring user {} has a RocketChat account".format(self.name))
        client = rocketchat.client(configuration)
        if not client.users_info(username=self.name):
            roles = ['guest']
            if Role.CUSTOMER not in self.roles:
                roles = ['user']
            user = client.users_create(
                email=self.email,
                name=self.display_name,
                password=self.password.secret,
                username=self.name,
                roles=roles,
                active=True,
                verified=True,
            ).json()
            if user.get('success'):
                self.rocketchat_id = user.get('user').get('_id')
                if Role.STAFF in self.roles:
                    for channel in ROCKETCHAT_CHANNELS:
                        group = client.groups_info(room_name=channel).json()
                        if group.get("success"):
                            group_id = group.get('group').get('_id')
                            client.groups_invite(
                                room_id=group_id,
                                user_id=self.rocketchat_id)
                        else:
                            raise Exception(group.get("error"))
                    if Role.PENTESTER in self.roles:
                        group = client.groups_info(room_name='ros-pentesters').json()
                        if group.get("success"):
                            group_id = group.get('group').get('_id')
                            client.groups_invite(
                                room_id=group_id,
                                user_id=self.rocketchat_id)
                        else:
                            raise Exception(group.get("error"))
            else:
                raise Exception(user.get("error"))

    def disable(self, configuration):
        self.disable_gitlab(configuration)
        self.disable_rocketchat(configuration)
        self.expire = datetime.now()

    def disable_gitlab(self, configuration):
        logging.info("Disabling {}'s Gitlab account".format(self.name))
        gitlab_client = gitlab.client(configuration)
        user = next(iter(gitlab_client.users.list(username=self.name)), None)
        if user is not None:
            user.block()

    def disable_rocketchat(self, configuration):
        logging.info("Disabling {}'s RocketChat account".format(self.name))
        client = rocketchat.client(configuration)
        user_id = client.users_info(username=self.name).json().get('user').get('_id')
        if user_id is not None:
            client.users_update(user_id=user_id, active=False)

    def enable(self, configuration):
        self.enable_gitlab(configuration)
        self.enable_rocketchat(configuration)
        self.expire = None

    def enable_gitlab(self, configuration):
        logging.info("Enabling {}'s Gitlab account".format(self.name))
        gitlab_client = gitlab.client(configuration)
        user = next(iter(gitlab_client.users.list(username=self.name)), None)
        if user is not None:
            user.unblock()

    def enable_rocketchat(self, configuration):
        logging.info("Enabling {}'s RocketChat account".format(self.name))
        client = rocketchat.client(configuration)
        user_id = client.users_info(username=self.name).json().get('user').get('_id')
        if user_id is not None:
            client.users_update(user_id=user_id, active=True)


class LoggedIn:
    """
    Enters a context in which the given user is logged in.
    """

    def __init__(self, auth, user):
        """
        Initiates the context and logs in the given user.
        :param user: User
        """
        self._auth = auth
        assert isinstance(user, User)
        self._auth.log_in(user)
        self._user = user

    def __enter__(self):
        return self._user

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._auth.log_out()


class Auth:
    def __init__(self, db):
        self._db = db
        self._current_user = None

    @property
    def current_user(self):
        return self._current_user

    def authenticate(self, username, password):
        """
        Gets a user by their username and password.

        :param username: str
        :param password: str
        :return: Optional[User]
        """
        # The below query has a noqa on it because the sqlalchemy magic
        # that handles (User.expire == None) cannot handle (User.expire is None)
        user = self._db.session.query(User) \
            .filter(User.name == username) \
            .filter(or_(User.expire == None, User.expire > datetime.now())) \
            .first()  # noqa
        if user and user.password == password:
            user.last_login = datetime.now()
            self._db.session.add(user)
            self._db.session.commit()
            return user
        return None

    def log_in(self, user):
        """
        Logs in the given user.

        :param user: User
        :return:
        """
        self._current_user = user

    def log_out(self):
        """
        Logs out the current user, if any.
        :return:
        """
        self._current_user = None

    def logged_in(self, user):
        """
        Enters a context in which the given user is logged in.
        """
        return LoggedIn(self, user)

    @staticmethod
    def can(permission):
        """
        Decorates a function to check if the current user has the given permission.

        This decorator only works for methods on instances with an _auth attribute that is an instance of Auth.
        """

        def decorator(fn):
            @wraps(fn)
            def permission_check(self, *args, **kwargs):
                assert isinstance(self._auth, Auth)
                user = self._auth.current_user
                if user is None:
                    raise NotAuthenticatedError()
                if user.cannot(permission):
                    raise NotAuthorizedError()
                return fn(self, *args, **kwargs)

            return permission_check

        return decorator
