import re
from datetime import datetime, date
from enum import Enum
from functools import lru_cache

import requests
from lxml import etree
from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.types import Enum as EnumColumn

from ros.helpers import locale_to_float
from ros.merge import merge
from ros.model import Base, Serializable


class ProjectType(Enum):
    QUOTE = 'off'
    PENTEST = 'pen'
    SALES = 'sales'
    BUSINESS = 'biz'
    INTERNAL = 'ros'


PROJECT_TYPE_LABELS = {
    ProjectType.PENTEST: 'Pentest',
    ProjectType.QUOTE: 'Quote',
    ProjectType.SALES: 'Sales',
    ProjectType.BUSINESS: 'Business',
    ProjectType.INTERNAL: 'Internal',
}


class Project(Base, Serializable):
    __tablename__ = 'project'
    id = Column('id', Integer, primary_key=True, info={
        'label': 'ID',
    })
    name = Column('name', String(255), nullable=False, info={
        'label': 'Name',
    })
    type = Column('type', EnumColumn(ProjectType), nullable=False, info={
        'label': 'Type',
    })
    created = Column('created', DateTime, nullable=False, default=lambda: datetime.now(), info={
        'label': 'Created',
    })
    rocketchat_id = Column('rocketchat_id', String(50), info={
        'label': 'RocketChat ID',
    })
    rocketchat_room_name = Column('rocketchat_room_name', String(255), info={
        'label': 'RocketChat room name',
    })
    gitlab_repository_name = Column('gitlab_repository_name', String(255), info={
        'label': 'GitLab repository name',
    })
    gitlab_id = Column('gitlab_id', Integer, info={
        'label': 'GitLab ID',
    })

    def __init__(self, project_type, name):
        assert project_type in ProjectType
        self.name = name
        self.type = project_type
        self.created = datetime.now()

    def serialize(self):
        return merge(Serializable.serialize(self), {
            'type': self.type.name,
            'name': self.name,
            'rocketchat-room-name': self.rocketchat_room_name,
        })

    @property
    def chat_url(self):
        return 'https://chat.radicallyopensecurity.com/group/%s' % self.rocketchat_room_name

    @property
    def repository_url(self):
        return 'https://gitlabs.radicallyopensecurity.com/ros/%s' % self.gitlab_repository_name

    @property
    @lru_cache(maxsize=None)
    def _report(self):
        url = 'https://gitlabs.radicallyopensecurity.com/ros/{}/raw/master/source/report.xml'.format(
            self.gitlab_repository_name)
        response = requests.get(url, timeout=3)
        response.raise_for_status()
        return etree.fromstring(
            response.text.replace('<?xml version="1.0" encoding="UTF-8"?>', '<?xml version="1.0"?>'))

    def _parse_xml_date(self, element):
        value = str(element.text)
        if re.fullmatch(r'^([0-9]){4}-([0-9]){2}-([0-9]){2}$', value):
            return date(*map(int, value.split('-')))

    @property
    def start(self):
        if self.type == ProjectType.PENTEST:
            return self._parse_xml_date(self._report.find('./meta/activityinfo/planning/start'))

    @property
    def end(self):
        if self.type == ProjectType.PENTEST:
            return self._parse_xml_date(self._report.find('./meta/activityinfo/planning/end'))

    @property
    def budgeted_hours(self):
        if self.type == ProjectType.PENTEST:
            element = self._report.find('./meta/activityinfo/persondays')
            if element.text:
                return locale_to_float(element.text) * 8
