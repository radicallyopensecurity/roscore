import argparse
from datetime import datetime

from ros.auth import User
from ros.charge import Charge
from ros.cli.args import (
    add_configuration_to_args,
    parse_configuration_from_args,
    add_messenger_to_args,
    parse_messenger_from_args,
    date,
)
from ros.db import Database
from ros.project import Project


def main(args):
    parser = argparse.ArgumentParser(
        description="Charge hours worked on projects.",
        usage='%(prog)s -t HOURS -d "description string" '
        "[-r ROCKETCHAT ROOMNAME if in DM] [-b BACKDATE YYYY-MM-DD if other"
        "than now]",
    )
    add_configuration_to_args(parser)
    add_messenger_to_args(parser)

    projectgroup = parser.add_mutually_exclusive_group(required=True)

    projectgroup.add_argument(
        "--rocketchat-room-name",
        "-r",
        action="store",
        help="The Rocketchat room name to charge hours to. "
        "(Mutually exclusive with --project-id)",
    )
    projectgroup.add_argument(
        "--project-id",
        action="store",
        type=int,
        help="The ID of the project to charge hours to. "
        "(Mutually exclusive with --rocketchat-room-name)",
    )

    usergroup = parser.add_mutually_exclusive_group(required=True)

    usergroup.add_argument(
        "--rocketchat-user-id",
        action="store",
        help="The Rocketchat ID of the user to log hours for. "
        "(Mutually exclusive with --user-id)",
    )
    usergroup.add_argument(
        "--user-id",
        action="store",
        type=int,
        help="The ID of the user to log hours for. "
        "(Mutually exclusive with --rocketchat-user-id)",
    )

    parser.add_argument(
        "--hours",
        "-t",
        action="store",
        type=float,
        help="The number of hours to charge (expressed decimally, "
        "so 1.5 records 1 hour and 30 minutes).",
        required=True,
    )
    # note: 'source' isn't used yet, is just here for future
    # feedback functionality w/r/t budgeted hours
    parser.add_argument(
        "--source",
        action="store",
        help="Was charge called in public or private channel",
        required=True,
        choices={"public", "private"},
    )
    parser.add_argument(
        "--description",
        "-d",
        action="store",
        help="An optional description of what the hours were spent "
        "on (surrounded with single or double quotes)",
    )
    parser.add_argument(
        "--date",
        "-b",
        action="store",
        default=datetime.strftime(datetime.now(), "%Y-%m-%d"),
        type=date,
        help="Date (YYYY-MM-DD) for backdating charge (if other than now)",
        required=False,
    )

    cli_args = parser.parse_args(args)

    configuration = parse_configuration_from_args(cli_args)
    messenger = parse_messenger_from_args(configuration, cli_args)

    db_url = configuration["database"]["url"]
    with Database(db_url) as db:
        if cli_args.rocketchat_user_id:
            user = (
                db.session.query(User)
                .filter(User.rocketchat_id == cli_args.rocketchat_user_id)
                .first()
            )
        if cli_args.user_id:
            user = (
                db.session.query(User)
                .filter(User.id == cli_args.user_id)
                .first()
            )
        if not user:
            raise ValueError(
                "User {} does not exist.".format(cli_args.user_id)
            )

        if cli_args.rocketchat_room_name:
            project = (
                db.session.query(Project)
                .filter(
                    Project.rocketchat_room_name
                    == cli_args.rocketchat_room_name
                )
                .first()
            )
        if cli_args.project_id:
            project = (
                db.session.query(Project)
                .filter(Project.id == cli_args.project_id)
                .first()
            )
        if not project:
            raise ValueError(
                "Project {} does not exist.".format(cli_args.project_id)
            )

        charge = Charge(user, project, cli_args.date, cli_args.hours)

        if cli_args.description:
            charge.description = cli_args.description

        db.session.add(charge)
        db.session.commit()

        messenger.confirm(
            "Charged {} hours to {}. Thanks for charging!".format(
                cli_args.hours, project.name
            )
        )
