import argparse
import os
from datetime import datetime
from functools import reduce, partial

from ros import messenger as ros_messenger
from ros.config import from_file, validate
from ros.merge import merge
from ros.messenger import GroupedMessengers, StdioMessenger


class ConfigurationPathAction(argparse.Action):
    """Provide a configuration file or directory path action."""

    def __call__(self, parser, namespace, values, option_string=None):
        """Invoke the action."""
        path = values
        if namespace.configuration is None:
            namespace.configuration = []

        if os.path.isfile(path):
            with open(path) as f:
                namespace.configuration.append(from_file(f))
        elif os.path.isdir(path):
            for file_path in sorted(filter(os.path.isfile, map(partial(os.path.join, path), os.listdir(path)))):
                with open(file_path) as f:
                    namespace.configuration.append(from_file(f))
        else:
            raise ValueError('`%s` is not an accessible file or directory.' % path)


def add_configuration_to_args(parser):
    """
    Adds an application configuration argument.

    :param parser: argparse.ArgumentParser
    :return: argparse.ArgumentParser
    """
    group = parser.add_argument_group('Configuration',
                                      'Multiple configuration options may be specified, with later ones being merged into earlier ones.')
    group.add_argument('--configuration-path', action=ConfigurationPathAction, dest='configuration', nargs='?',
                       help='The path to a configuration file or directory.')
    group.set_defaults(configuration_file_path=[], configuration_json=[])
    return parser


def parse_configuration_from_args(args):
    """
    Parses application configuration from argparse arguments.

    :param args: argparse.Namespace
    :return: Dict
    :raise: ros.config.ConfigurationError
    """
    if not args.configuration:
        raise ValueError('At least one --configuration-* option must be specified.')

    configuration = reduce(merge, args.configuration)
    validate(configuration)

    return configuration


def add_messenger_to_args(parser):
    """
    Adds optional messenger arguments.

    :param parser: ArgumentParser
    :return: ArgumentParser
    """
    group = parser.add_argument_group('Messaging',
                                      'Arguments to configure how ROS will communicate progress, successes, and failures.')
    group.add_argument('--messenger-rocketchat-room', action='store',
                       help='The name of the RocketChat room to send user-facing messages to.')
    return parser


def parse_messenger_from_args(configuration, args):
    """
    Parses the messenger from argparse arguments and application configuration.

    :param: configuration: Dict
    :param args: argparse.Namespace
    :return: Dict
    :raise: ros.config.ConfigurationError
    """
    messenger = GroupedMessengers()
    messenger.add_messenger(StdioMessenger())
    if args.messenger_rocketchat_room is not None and 'rocketchat' in configuration:
        messenger.add_messenger(ros_messenger.RosbotMessenger(configuration['rocketchat']['url'], args.messenger_rocketchat_room))
    return messenger


def date(value):
    return datetime.strptime(value, "%Y-%m-%d")
