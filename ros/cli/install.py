import argparse

from ros.cli.args import add_configuration_to_args, parse_configuration_from_args, parse_messenger_from_args, \
    add_messenger_to_args
from ros.config import Bootstrapped
from ros.db import install, Database


def main(args):
    parser = argparse.ArgumentParser(args)
    add_configuration_to_args(parser)
    add_messenger_to_args(parser)
    cli_args = parser.parse_args()
    configuration = parse_configuration_from_args(cli_args)
    messenger = parse_messenger_from_args(configuration, cli_args)
    db_url = configuration['database']['url']
    messenger.inform('Installing ROS into %s' % db_url)
    with Bootstrapped(configuration, messenger):
        with Database(db_url) as db:
            install(db)
        messenger.confirm('Done installing ROS into %s' % db_url)
