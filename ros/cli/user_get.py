import argparse
import json

from ros.auth import User
from ros.cli.args import add_configuration_to_args, parse_configuration_from_args, parse_messenger_from_args, \
    add_messenger_to_args
from ros.config import Bootstrapped
from ros.db import Database


def main(args):
    parser = argparse.ArgumentParser(description='Query, filter, and output ROS users.')
    add_configuration_to_args(parser)
    add_messenger_to_args(parser)
    parser.add_argument('--rocketchat-id', action='store', help='The RocketChat ID to filter users by.')
    cli_args = parser.parse_args(args)
    configuration = parse_configuration_from_args(cli_args)
    messenger = parse_messenger_from_args(configuration, cli_args)
    db_url = configuration['database']['url']
    with Bootstrapped(configuration, messenger):
        with Database(db_url) as db:
            query = db.session.query(User)
            if cli_args.rocketchat_id:
                query = query.filter(User.rocketchat_id == cli_args.rocketchat_id)
            print(json.dumps([user.serialize() for user in query]))
