import argparse
import json

from ros.cli.args import add_configuration_to_args, parse_configuration_from_args, parse_messenger_from_args, \
    add_messenger_to_args
from ros.config import Bootstrapped
from ros.db import Database
from ros.project import Project


def main(args):
    parser = argparse.ArgumentParser(description='Query, filter, and output ROS projects.')
    add_configuration_to_args(parser)
    add_messenger_to_args(parser)
    parser.add_argument('--rocketchat-room', action='store', help='The RocketChat room name to filter projects by.')
    cli_args = parser.parse_args(args)
    configuration = parse_configuration_from_args(cli_args)
    messenger = parse_messenger_from_args(configuration, cli_args)
    db_url = configuration['database']['url']
    with Bootstrapped(configuration, messenger):
        with Database(db_url) as db:
            query = db.session.query(Project)
            if cli_args.rocketchat_room:
                query = query.filter(Project.rocketchat_room_name == cli_args.rocketchat_room)
            print(json.dumps([project.serialize() for project in query]))
