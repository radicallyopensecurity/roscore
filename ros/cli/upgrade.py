import argparse

from ros.cli.args import add_configuration_to_args, parse_configuration_from_args, add_messenger_to_args, \
    parse_messenger_from_args
from ros.config import Bootstrapped
from ros.db import upgrade, Database


def main():
    parser = argparse.ArgumentParser()
    add_configuration_to_args(parser)
    add_messenger_to_args(parser)
    cli_args = parser.parse_args()
    configuration = parse_configuration_from_args(cli_args)
    messenger = parse_messenger_from_args(configuration, cli_args)
    db_url = configuration['database']['url']
    messenger.inform('Upgrading ROS in %s' % db_url)
    with Bootstrapped(configuration, messenger):
        with Database(db_url) as db:
            upgrade(db)
    messenger.confirm('Done upgrading ROS in %s' % db_url)
