import argparse

from ros.cli.args import add_configuration_to_args, parse_configuration_from_args, add_messenger_to_args, \
    parse_messenger_from_args
from ros.config import Bootstrapped
from ros.migrate.migrate_1_to_2 import migrate


def main(args):
    parser = argparse.ArgumentParser()
    add_configuration_to_args(parser)
    add_messenger_to_args(parser)
    parser.add_argument('--ros-1-db-url-rosbotuser', action='store', dest='ros_1_db_url_rosbotuser', required=True,
                        help='The URL to the old ROS 1 rosbotuser database.')
    parser.add_argument('--ros-1-db-url-planning', action='store', dest='ros_1_db_url_planning', required=True,
                        help='The URL to the old ROS 1 planning database.')
    cli_args = parser.parse_args(args)
    configuration = parse_configuration_from_args(cli_args)
    messenger = parse_messenger_from_args(configuration, cli_args)
    ros_1_db_url_rosbotuser = cli_args.ros_1_db_url_rosbotuser
    ros_1_db_url_planning = cli_args.ros_1_db_url_planning
    ros_2_db_url = configuration['database']['url']
    messenger.inform('Migrating ROS 1 to ROS 2 at %s' % ros_2_db_url)
    with Bootstrapped(configuration, messenger):
        migrate(configuration, messenger, ros_1_db_url_rosbotuser, ros_1_db_url_planning)
        messenger.confirm('Done migrating ROS 1 to ROS 2 at %s' % ros_2_db_url)
