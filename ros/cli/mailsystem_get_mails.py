import imaplib
import ssl
import email
import argparse

from ros.mailsystem import Email
from dateutil.parser import parse
from flanker.addresslib import address

from ros.cli.args import add_configuration_to_args, parse_configuration_from_args
from ros.db import Database


def main(args):
    parser = argparse.ArgumentParser(description='Gets emails from inbox and puts them in the database.')
    add_configuration_to_args(parser)
    cli_args = parser.parse_args(args)
    configuration = parse_configuration_from_args(cli_args)

    db_url = configuration['database']['url']
    email_server = configuration['mailsystem']['email_server']
    email_folder = configuration['mailsystem']['email_folder']
    email_user = configuration['mailsystem']['email_user']
    email_pass = configuration['mailsystem']['email_pass']

    with Database(db_url) as db:
        tls_context = ssl.create_default_context()
        server = imaplib.IMAP4(email_server)
        server.starttls(ssl_context=tls_context)

        server.login(email_user, email_pass)

        server.select(email_folder)

        _result, data = server.search(None, 'ALL')

        for num in data[0].split():
            _typ, data = server.fetch(num, '(RFC822)')
            msg = email.message_from_bytes(data[0][1])
            uuid = email.header.make_header(email.header.decode_header(msg['Message-ID']))
            frommail = email.header.make_header(email.header.decode_header(msg['From']))
            subject = email.header.make_header(email.header.decode_header(msg['Subject']))
            date = email.header.make_header(email.header.decode_header(msg['Date']))
            exists = db.session.query(Email).filter_by(server_id=str(uuid)).scalar() is not None

            if not exists:
                print('==NEW EMAIL==')
                print(uuid)
                print(frommail)
                print(address.parse(str(frommail)))
                print(subject)
                print(date)

                e = Email(
                    server_id=str(uuid),
                    sender_name=str(frommail),
                    sender_address=str(address.parse(str(frommail))),
                    subject=str(subject),
                    datetime=parse(str(date)))
                db.session.add(e)

            # move to processed folder
            server.uid('COPY', num, email_folder + '.processed')

            # server.store(num, '+FLAGS', '\\Deleted')
            # server.expunge()

        server.close()
        db.session.commit()
