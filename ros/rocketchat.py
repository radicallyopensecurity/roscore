from rocketchat_API.rocketchat import RocketChat


def client(configuration):
    """
    Create a new RocketChat client based on ROS configuration.

    :return: rocketchat.Rocketchat
    """
    return RocketChat(configuration['rocketchat']['user'],
                      configuration['rocketchat']['password'],
                      server_url=configuration['rocketchat']['url'],
                      )
