class ProgressBar:
    def __init__(self, max_value, current_value):
        self._max = max_value
        self._current = current_value

    def __str__(self):
        percentage = self._current / self._max * \
                     100 if self._current / self._max * 100 <= 100 else 100
        actual_percentage = self._current / self._max * 100
        bar = ''
        for i in range(int(percentage)):
            bar += ':'
        for i in range(100 - int(percentage)):
            bar += '.'
        return '[{}] [{}%]'.format(bar, round(actual_percentage, 2))
