from datetime import datetime

from sqlalchemy import Column, Integer, ForeignKey, DateTime, Float, Text, Date
from sqlalchemy.orm import relationship

from ros.auth import User
from ros.model import Base
from ros.project import Project


class Charge(Base):
    __tablename__ = "charge"
    id = Column("id", Integer, primary_key=True)
    _user_id = Column("user_id", Integer, ForeignKey(User.id))
    user = relationship(User)
    _project_id = Column("project_id", Integer, ForeignKey(Project.id))
    project = relationship(Project)
    created = Column("created", DateTime)
    date = Column("chargetime", Date)
    # @todo 'hours' should not be a float; it can cause discrepancies,
    # which we don't want because this is about money.
    hours = Column("hours", Float)
    description = Column("description", Text)

    def __init__(self, user, project, date, hours):
        self.user = user
        self.project = project
        self.created = datetime.now()
        self.date = date
        self.hours = hours
