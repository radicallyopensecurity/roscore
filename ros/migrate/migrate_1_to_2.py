from sqlalchemy_utils import Password

from ros.auth import User, Role
from ros.db import Database
from ros.project import Project, ProjectType


def migrate(configuration, messenger, ros_1_db_url_rosbotuser, ros_1_db_url_planning):
    _migrate_users(configuration, messenger, ros_1_db_url_rosbotuser)
    _migrate_projects(configuration, messenger, ros_1_db_url_planning)


def _migrate_users(configuration, messenger, ros_1_db_url_rosbotuser):
    messenger.inform('Migrating users from the `user` and `user_role` tables in %s to the new user model.' % ros_1_db_url_rosbotuser)

    users = []
    rosbotuser_user_role_map = {
        1: Role.STAFF,
        2: Role.CUSTOMER,
        3: Role.PENTESTER,
        4: Role.WRITER,
        5: Role.SYSOP,
        6: Role.PM,
        7: Role.MEMBER_NEW_OFFERTES,
        8: Role.MEMBER_NEW_PENTESTS,
        9: Role.VOLUNTEER,
    }

    # Convert data from the old database to new user entities.
    with Database(ros_1_db_url_rosbotuser) as ros_1_db:
        for user_data in ros_1_db.connection.execute('SELECT * FROM user').fetchall():
            # Map the `user` table.
            user = User(user_data.user_name, Password(user_data.password), user_data.email)
            user.id = int(user_data.user_id)
            user.display_name = user_data.realname
            user.expire = user_data.expire
            user.last_login = user_data.last_login
            user.gitlab_id = user_data.gitlab_id
            user.rocketchat_id = user_data.rocketchat_id

            # Map the `user_role` table.
            rosbotuser_user_roles_data = ros_1_db.connection.execute('SELECT role_id FROM user_role WHERE user_id = %d' % user.id)
            for rosbotuser_user_role_data in rosbotuser_user_roles_data.fetchall():
                user.roles.append(rosbotuser_user_role_map[rosbotuser_user_role_data.role_id])

            users.append(user)

    # Store the new user entities.
    with Database(configuration['database']['url']) as ros_2_db:
        session = ros_2_db.session
        for user in users:
            session.add(user)
        session.commit()
    messenger.confirm('Completed user migration.')

    # Test the migration on a single user.
    messenger.inform('Testing user migration.')
    with Database(ros_1_db_url_rosbotuser) as ros_1_db:
        john_id = 2
        john_data = ros_1_db.connection.execute("SELECT * FROM user WHERE user_id = %s", john_id).fetchone()
        with Database(configuration['database']['url']) as ros_2_db:
            john = ros_2_db.session.query(User).filter(User.id == john_id).first()
            assert john.id == john_data.user_id
            assert john.display_name == john_data.realname
            assert john.email == john_data.email
            assert john.expire == john_data.expire
            assert john.last_login == john_data.last_login
            assert john.gitlab_id == john_data.gitlab_id
            assert john.rocketchat_id == john_data.rocketchat_id
            # Test user roles.
            john_user_roles_data = ros_1_db.connection.execute('SELECT role_id FROM user_role WHERE user_id = %d' % john_id)
            for john_user_role_data in john_user_roles_data.fetchall():
                assert rosbotuser_user_role_map[john_user_role_data.role_id] in john.roles
    messenger.confirm('Confirmed user migration was successful.')


def _migrate_projects(configuration, messenger, ros_1_db_url_planning):
    messenger.inform('Migrating projects from the `triad` table in %s to the new project model.' % ros_1_db_url_planning)
    projects = []
    planning_triad_prefix_map = {
        'sales': ProjectType.SALES,
        'biz': ProjectType.BUSINESS,
        'pen': ProjectType.PENTEST,
        'off': ProjectType.QUOTE,
    }

    # Convert data from the old database to new project entities.
    with Database(ros_1_db_url_planning) as ros_1_db:
        # Map the `triad` table.
        for project_data in ros_1_db.connection.execute('SELECT * FROM triad').fetchall():
            # Some `test` projects exist in the database, which do not need to be migrated.
            if project_data.prefix == 'test':
                continue

            project = Project(planning_triad_prefix_map[project_data.prefix], project_data.name)
            project.id = int(project_data.id)
            project.rocketchat_room_name = '%s-%s' % (project_data.prefix, project_data.name)
            project.rocketchat_id = project_data.rocketchat_id
            project.gitlab_repository_name = '%s-%s' % (project_data.prefix, project_data.name)
            project.gitlab_id = project_data.gitlab_id

            projects.append(project)

    # Store the new project entities.
    with Database(configuration['database']['url']) as ros_2_db:
        session = ros_2_db.session
        for project in projects:
            session.add(project)
        session.commit()
    messenger.confirm('Completed project migration.')

    # Test the migration on a single project.
    messenger.inform('Testing project migration.')
    with Database(ros_1_db_url_planning) as ros_1_db:
        project_id = 7
        project_data = ros_1_db.connection.execute("SELECT * FROM triad WHERE id = %s", project_id).fetchone()
        with Database(configuration['database']['url']) as ros_2_db:
            project = ros_2_db.session.query(Project).filter(Project.id == project_id).first()
            assert project is not None
            assert project.type == planning_triad_prefix_map[project_data.prefix]
            assert project.name == project_data.name
            assert project.rocketchat_room_name == '%s-%s' % (project_data.prefix, project_data.name)
            assert project.rocketchat_id == project_data.rocketchat_id
            assert project.gitlab_repository_name == '%s-%s' % (project_data.prefix, project_data.name)
            assert project.gitlab_id == project_data.gitlab_id
    messenger.confirm('Confirmed project migration was successful.')
