from unittest import TestCase

from parameterized import parameterized

from ros.helpers import locale_to_float
from ros.helpers import render_template


class HelpersTest(TestCase):
    @parameterized.expand([
        ('1234.5', 1234.5),
        ('12.345', 12.345),
        ('12345', 12345),
    ])
    def test_locale_to_float(self, input, expected):
        self.assertEqual(locale_to_float(input), expected)

    @parameterized.expand([
        ('',),
        ('.',),
        (',',),
        ('1234,5',),
    ])
    def test_locale_to_float_should_raise_error(self, input):
        with self.assertRaises(ValueError):
            locale_to_float(input)

    @parameterized.expand([
        ('hello, %name%', {'name': 'test'}, 'hello, test'),
        ('hello, %name', {'name': 'test'}, 'hello, %name'),
    ])
    def test_render_template(self, input, opts, expected):
        self.assertEqual(render_template(input, opts), expected)
