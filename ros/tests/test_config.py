import json
from logging import Logger, NOTSET
from tempfile import NamedTemporaryFile
from unittest import TestCase
from unittest.mock import patch, Mock

import yaml
from parameterized import parameterized

from ros.config import from_file, ConfigurationValueError, ConfigurationFileError, Bootstrapped
from ros.messenger import Messenger


class FromFile(TestCase):
    @parameterized.expand([
        (json.dump, 'json'),
        (yaml.dump, 'yml'),
        (yaml.dump, 'yaml'),
    ])
    def test_with_valid_values_should_parse_config(self, dumper, extension):
        expected_configuration = {
            'database': {
                'url': 'rosql://bart:*******@https://example.com:3306/ros',
            },
            'flask': {},
            'gitlab': {
                'url': 'https://example.com',
                'token': 'foo',
                'namespace': 'bar',
                'user': {
                    'name': 'ros',
                    'email': 'ros@example.com',
                }
            },
            'smtp-server': 'mail',
        }
        with NamedTemporaryFile(mode='w+t', suffix='.%s' % extension) as f:
            dumper(expected_configuration, f)
            f.seek(0)
            actual_configuration = from_file(f)
        self.assertEquals(actual_configuration, expected_configuration)

    @parameterized.expand([
        (json.dump, 'json'),
        (yaml.dump, 'yml'),
        (yaml.dump, 'yaml'),
    ])
    def test_with_invalid_values_should_error(self, dumper, extension):
        configuration = {}
        with NamedTemporaryFile(mode='w+t', suffix='.%s' % extension) as f:
            dumper(configuration, f)
            f.seek(0)
            with self.assertRaises(ConfigurationValueError):
                from_file(f)

    def test_with_invalid_format_should_error(self):
        with NamedTemporaryFile(mode='w+t', suffix='.blub') as f:
            with self.assertRaises(ConfigurationFileError):
                from_file(f)


class BootstrappedTest(TestCase):
    CONFIGURATION = {
        'database': {
            'url': 'rosql://bart:*******@https://example.com:3306/ros',
        },
        'flask': {},
        'gitlab': {
            'url': 'https://example.com',
            'token': 'foo',
            'namespace': 'bar',
            'user': {
                'name': 'ros',
                'email': 'ros@example.com',
            },
        }
    }

    def test_messenger(self):
        messenger = Mock(Messenger)
        message = 'Something happened!'
        with Bootstrapped(self.CONFIGURATION, messenger) as ros:
            ros.messenger.alert(message)
        messenger.alert.assert_called_with(message)

    def test_logger(self):
        messenger = Mock(Messenger)
        with Bootstrapped(self.CONFIGURATION, messenger) as ros:
            ros_logger = ros.logger
        self.assertEquals(ros_logger.name, 'ros')

    @patch('logging.config.dictConfig')
    def test_logging_configuration(self, m_dict_config):
        configuration = self.CONFIGURATION.copy()
        configuration.update({
            'logging': {
                'version': 1,
                'handlers': {
                    __name__: {
                        'class': 'logging.FileHandler',
                        'filename': '/tmp/foo',
                    },
                },
                'loggers': {
                    'ros': {
                        'handlers': [__name__],
                    },
                },
            },
        })
        messenger = Mock(Messenger)
        with Bootstrapped(configuration, messenger):
            pass
        m_dict_config.assert_called_with(configuration['logging'])

    @patch('logging.getLogger')
    def test_with_interrupt(self, m_get_logger):
        result = False
        messenger = Mock(Messenger)
        m_logger = Mock(Logger)
        m_logger.handlers = Mock(side_effect=lambda: [])
        m_logger.getEffectiveLevel.side_effect = Mock(
            side_effect=lambda: NOTSET)
        m_get_logger.return_value = m_logger
        with Bootstrapped(self.CONFIGURATION, messenger):
            raise KeyboardInterrupt()
            result = True
        self.assertFalse(result)
        self.assertFalse(m_logger.exception.called)

    @patch('logging.getLogger')
    def test_with_exception(self, m_get_logger):
        result = False
        messenger = Mock(Messenger)
        m_logger = Mock(Logger)
        m_logger.handlers = Mock(side_effect=lambda: [])
        m_logger.getEffectiveLevel.side_effect = Mock(
            side_effect=lambda: NOTSET)
        m_get_logger.return_value = m_logger
        with Bootstrapped(self.CONFIGURATION, messenger):
            raise RuntimeError()
            result = True
        self.assertFalse(result)
        self.assertTrue(m_logger.exception.called)

    def test_without_exception(self):
        messenger = Mock(Messenger)
        with Bootstrapped(self.CONFIGURATION, messenger):
            result = True
        self.assertTrue(result)
