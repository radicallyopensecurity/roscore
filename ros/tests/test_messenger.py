from unittest import TestCase
from unittest.mock import Mock, call, patch

from parameterized import parameterized

from ros.messenger import StdioMessenger, GroupedMessengers


class GroupedMessengersTest(TestCase):
    def test_state(self):
        messenger_1 = Mock()
        messenger_2 = Mock()
        messenger_3 = Mock()
        sut = GroupedMessengers()
        sut.add_messenger(messenger_1)
        sut.add_messenger(messenger_2)
        sut.add_messenger(messenger_3)
        message = 'Something happened!'
        sut.state(message)
        messenger_1.state.assert_called_with(message)
        messenger_2.state.assert_called_with(message)
        messenger_3.state.assert_called_with(message)

    def test_inform(self):
        messenger_1 = Mock()
        messenger_2 = Mock()
        messenger_3 = Mock()
        sut = GroupedMessengers()
        sut.add_messenger(messenger_1)
        sut.add_messenger(messenger_2)
        sut.add_messenger(messenger_3)
        message = 'Something happened!'
        sut.inform(message)
        messenger_1.inform.assert_called_with(message)
        messenger_2.inform.assert_called_with(message)
        messenger_3.inform.assert_called_with(message)

    def test_confirm(self):
        messenger_1 = Mock()
        messenger_2 = Mock()
        messenger_3 = Mock()
        sut = GroupedMessengers()
        sut.add_messenger(messenger_1)
        sut.add_messenger(messenger_2)
        sut.add_messenger(messenger_3)
        message = 'Something happened!'
        sut.confirm(message)
        messenger_1.confirm.assert_called_with(message)
        messenger_2.confirm.assert_called_with(message)
        messenger_3.confirm.assert_called_with(message)

    def test_alert(self):
        messenger_1 = Mock()
        messenger_2 = Mock()
        messenger_3 = Mock()
        sut = GroupedMessengers()
        sut.add_messenger(messenger_1)
        sut.add_messenger(messenger_2)
        sut.add_messenger(messenger_3)
        message = 'Something happened!'
        sut.alert(message)
        messenger_1.alert.assert_called_with(message)
        messenger_2.alert.assert_called_with(message)
        messenger_3.alert.assert_called_with(message)


class StdioMessengerTest(TestCase):
    @parameterized.expand([
        ('state', 7),
        ('inform', 6),
        ('confirm', 2),
        ('alert', 1),
    ])
    @patch('sys.stderr')
    def test(self, message_type, color, m):
        sut = StdioMessenger()
        message = 'Something happened!'
        getattr(sut, message_type)(message)
        bg_color = color + 40
        fg_color = color + 30
        m.write.assert_has_calls([
            call('\x1b[0;%dm  \x1b[0;1;%dm %s\x1b[0m' %
                 (bg_color, fg_color, message), ),
            call('\n', ),
        ])
