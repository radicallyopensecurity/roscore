from datetime import datetime, timedelta
from unittest import TestCase
from unittest.mock import patch, MagicMock

from ros.auth import User, Role, NotAuthenticatedError, NotAuthorizedError, Auth
from ros.db import Database, TemporaryDatabase


class AuthTest(TestCase):
    @patch('ros.db.Database', spec=Database)
    def test_current_user(self, m_db):
        auth = Auth(m_db)
        user = User('foo', 'bar', 'foo@example.com')
        self.assertIsNone(auth.current_user)
        auth.log_in(user)
        self.assertEquals(auth.current_user, user)
        auth.log_out()
        self.assertIsNone(auth.current_user)

    @patch('ros.db.Database', spec=Database)
    def test_logged_in(self, m_db):
        auth = Auth(m_db)
        user = User('foo', 'bar', 'foo@example.com')
        self.assertIsNone(auth.current_user)
        with auth.logged_in(user):
            self.assertEquals(auth.current_user, user)
        self.assertIsNone(auth.current_user)

    @patch('ros.db.Database', spec=Database)
    def test_can(self, m_db):
        auth = Auth(m_db)
        user = User('foo', 'bar', 'foo@example.com')
        user.roles = [Role.STAFF]
        auth.log_in(user)

        class Foo:
            def __init__(self, auth):
                self._auth = auth

            @Auth.can('use.chat')
            def target(self):
                return 'FooBar'

        foo = Foo(auth)
        self.assertEquals(foo.target(), 'FooBar')

    @patch('ros.db.Database', spec=Database)
    def test_can_not_if_not_authenticated(self, m_db):
        auth = Auth(m_db)

        class Foo:
            def __init__(self, auth):
                self._auth = auth

            @Auth.can('use.chat')
            def target(self):
                return 'FooBar'

        foo = Foo(auth)

        with self.assertRaises(NotAuthenticatedError):
            foo.target()

    @patch('ros.db.Database', spec=Database)
    def test_can_not_if_not_authorized(self, m_db):
        auth = Auth(m_db)
        user = User('foo', 'bar', 'foo@example.com')
        auth.log_in(user)

        class Foo:
            def __init__(self, auth):
                self._auth = auth

            @Auth.can('use.chat')
            def target(self):
                return 'FooBar'

        foo = Foo(auth)

        with self.assertRaises(NotAuthorizedError):
            foo.target()

    @patch('ros.db.Database', spec=Database)
    def test_can_not_if_expired(self, m_db):
        auth = Auth(m_db)
        user = User('foo', 'bar', 'foo@example.com')
        user.expire = datetime.now() - timedelta(hours=1)
        user.roles = [Role.STAFF]
        auth.log_in(user)

        class Foo:
            def __init__(self, auth):
                self._auth = auth

            @Auth.can('use.chat')
            def target(self):
                return 'FooBar'

        foo = Foo(auth)

        with self.assertRaises(NotAuthorizedError):
            foo.target()

    def test_authenticate(self):
        with TemporaryDatabase() as db:
            auth = Auth(db)
            username = 'Hans'
            password = 'Numbers'
            user = User(username, password, 'hans@example.com')
            db.session.add(user)
            db.session.commit()
            self.assertIsNone(user.last_login)
            self.assertEquals(auth.authenticate(username, password), user)

            # Ensure the last login time was updated correctly.
            last_login_allowed_delay = 3
            authenticated_user = db.session.query(User).filter(User.id == user.id).first()
            self.assertIsNotNone(authenticated_user.last_login)
            last_login_ago = datetime.now() - authenticated_user.last_login
            self.assertLess(last_login_ago.total_seconds(), last_login_allowed_delay)

    def test_authenticate_with_incorrect_password(self):
        with TemporaryDatabase() as db:
            auth = Auth(db)
            username = 'Hans'
            password = 'Numbers'
            user = User(username, password, 'hans@example.com')
            db.session.add(user)
            db.session.commit()
            self.assertIsNone(auth.authenticate(username, 'NotNumbers'))

    def test_authenticate_with_unknown_user(self):
        with TemporaryDatabase() as db:
            auth = Auth(db)
            username = 'Hans'
            password = 'Numbers'
            user = User(username, password, 'hans@example.com')
            db.session.add(user)
            db.session.commit()
            self.assertIsNone(auth.authenticate('NotHans', password))

    def test_authenticate_expired(self):
        with TemporaryDatabase() as db:
            auth = Auth(db)
            username = 'Hans'
            password = 'Numbers'
            user = User(username, password, 'hans@example.com')
            user.expire = datetime.now() - timedelta(hours=1)
            db.session.add(user)
            db.session.commit()
            self.assertIsNone(auth.authenticate(username, password))


class UserTest(TestCase):
    def setUp(self):
        username = 'Bart'
        password = 'MySuperSecretPassword'
        email = 'bart@example.com'
        self.user = User(username, password, email)
        self.user.roles = [Role.STAFF]

    def test_serialize_minimal(self):
        user_id = 313
        name = 'Hans'
        email = 'hans@example.com'
        user = User(name, 'bl33p', email)
        user.id = user_id
        expected = {
            'id': user_id,
            'name': name,
            'email': email,
            'rocketchat-id': None,
            'gitlab-id': None,
            'roles': [],
        }
        actual = user.serialize()
        self.assertEqual(actual, expected)

    def test_serialize_full(self):
        user_id = 313
        name = 'Hans'
        email = 'hans@example.com'
        rocketchat_id = '313'
        gitlab_id = 666
        roles = [Role.STAFF, Role.PENTESTER]
        user = User(name, 'bl33p', email)
        user.id = user_id
        user.roles = roles
        user.rocketchat_id = rocketchat_id
        user.gitlab_id = gitlab_id
        expected = {
            'id': user_id,
            'name': name,
            'email': email,
            'rocketchat-id': rocketchat_id,
            'gitlab-id': gitlab_id,
            'roles': [role.name for role in roles],
        }
        actual = user.serialize()
        self.assertEqual(actual, expected)

    def test_can(self):
        self.assertTrue(self.user.can('use.chat'))

    def test_cannot(self):
        self.assertFalse(self.user.cannot('use.chat'))

    @patch('ros.auth.rocketchat')
    @patch('ros.auth.gitlab')
    @patch('ros.db.Database', spec=Database)
    def test_disable(self, _db, _gitlab, _rocketchat):
        gitlab_client = MagicMock()
        _gitlab.client.return_value = gitlab_client
        gl_user = MagicMock()
        gitlab_client.users.list.return_value = [gl_user]

        rc_client = MagicMock()
        info = MagicMock()
        rc_client.users_info.return_value = info
        info.json.return_value = {'user': {'_id': "test"}}
        _rocketchat.client.return_value = rc_client

        self.user.disable({})

        gl_user.block.assert_called_once_with()
        rc_client.users_update.assert_called_once_with(user_id='test', active=False)

    @patch('ros.auth.rocketchat')
    @patch('ros.auth.gitlab')
    @patch('ros.db.Database', spec=Database)
    def test_enable(self, _db, _gitlab, _rocketchat):
        gitlab_client = MagicMock()
        _gitlab.client.return_value = gitlab_client
        gl_user = MagicMock()
        gitlab_client.users.list.return_value = [gl_user]

        rc_client = MagicMock()
        info = MagicMock()
        rc_client.users_info.return_value = info
        info.json.return_value = {'user': {'_id': "test"}}
        _rocketchat.client.return_value = rc_client

        self.user.enable({})

        gl_user.unblock.assert_called_once_with()
        rc_client.users_update.assert_called_once_with(user_id='test', active=True)
