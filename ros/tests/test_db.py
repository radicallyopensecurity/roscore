from tempfile import NamedTemporaryFile
from unittest import TestCase

from ros.db import install, Database


class TestInstall(TestCase):
    def test_install(self):
        with NamedTemporaryFile() as f:
            db_url = 'sqlite:///%s' % f.name
            with Database(db_url) as db:
                install(db)
                # Test at least the user table was created.
                self.assertEquals(list(db.connection.execute("SELECT * FROM user")), [])
