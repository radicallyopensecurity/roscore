from unittest import TestCase

from parameterized import parameterized

from ros.merge import merge


class MergeTest(TestCase):
    @parameterized.expand([
        ('foo', 'foo', 'foo'),
        ('bar', 'foo', 'bar'),
        ([3, 2, 1], [1, 2, 3], [3, 2, 1]),
        ({
            'foo': 'foo',
            'bar': 'Bar',
            'baz': 'Baz',
         }, {
            'foo': 'foo',
            'bar': 'bar',
         }, {
            'bar': 'Bar',
            'baz': 'Baz',
         }),
    ])
    def test_with_valid_values_should_parse_config(self, expected, original, override):
        self.assertEquals(merge(original, override), expected)
