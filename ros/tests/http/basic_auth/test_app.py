from unittest import TestCase

import requests
import requests_mock
from basicauth import encode

from ros.auth import User, PERMISSIONS, Role
from ros.http.basic_auth.app import App
from ros.db import TemporaryDatabase


class BasicAuthTest(TestCase):
    def setUp(self):
        self._db = TemporaryDatabase()
        self._configuration = {
            'database': {
                'url': self._db.url,
            },
            'flask': {
                'DEBUG': True,
            },
            'gitlab': {
                'url': 'https://example.com',
                'token': 'foo',
                'namespace': 'bar',
                'user': {
                    'name': 'ros',
                    'email': 'ros@example.com',
                },
            },
            'rocketchat': {
                'url': 'https://example.com',
                'user': 'test',
                'password': 'test pass',
            }
        }
        self._flask_app = App(self._configuration)
        self._flask_app_context = self._flask_app.app_context()
        self._flask_app_context.push()
        self._flask_app_client = self._flask_app.test_client()

        session = requests.Session()
        adapter = requests_mock.Adapter()
        session.mount('mock', adapter)

    def tearDown(self):
        self._db.close()
        self._flask_app_context.pop()

    def _build_basic_auth_headers(self, name, password):
        return {
            'Authorization': encode(name, password),
        }

    def testWithNonExistentUserShould401(self):
        username = 'Bart'
        password = 'MySuperSecretPassword'
        headers = self._build_basic_auth_headers(username, password)
        response = self._flask_app_client.get('/auth/basic/foo', headers=headers)
        self.assertEquals(401, response.status_code)

    def testWithoutPermissionShould403(self):
        username = 'Bart'
        password = 'MySuperSecretPassword'
        email = 'bart@example.com'
        permission = PERMISSIONS[Role.STAFF][0]
        user = User(username, password, email)
        self._db.session.add(user)
        self._db.session.commit()
        headers = self._build_basic_auth_headers(username, password)
        response = self._flask_app_client.get('/auth/basic/%s' % permission, headers=headers)
        self.assertEquals(403, response.status_code)

    def testWithPermissionShould200(self):
        username = 'Bart'
        password = 'MySuperSecretPassword'
        email = 'bart@example.com'
        role = Role.STAFF
        permission = PERMISSIONS[role][0]
        user = User(username, password, email)
        user.roles.append(role)
        self._db.session.add(user)
        self._db.session.commit()
        headers = self._build_basic_auth_headers(username, password)
        response = self._flask_app_client.get('/auth/basic/%s' % permission, headers=headers)
        self.assertEquals(200, response.status_code)
