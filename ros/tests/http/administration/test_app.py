from datetime import datetime
from unittest import TestCase
from unittest.mock import patch

import requests
import requests_mock
from parameterized import parameterized
from werkzeug.datastructures import MultiDict

from ros.auth import User, Role
from ros.http.administration.app import App
from ros.db import Database, TemporaryDatabase
from ros.project import Project, ProjectType


class AdministrationTestCase(TestCase):
    def setUp(self):
        self._db = TemporaryDatabase()
        self._configuration = {
            'database': {
                'url': self._db.url,
            },
            'flask': {
                'DEBUG': True,
                'TESTING': True,
                'SECRET_KEY': 'MyFirstSecretKey',
                # @todo Find out how to generate CSRF tokens when testing form submissions.
                'WTF_CSRF_ENABLED': False,
            },
            'gitlab': {
                'url': 'https://example.com',
                'token': 'foo',
                'namespace': 'bar',
                'user': {
                    'name': 'ros',
                    'email': 'ros@example.com',
                },
            },
            'rocketchat': {
                'url': 'https://example.com',
                'user': 'test',
                'password': 'test pass',
            }
        }
        self._flask_app = App(self._configuration)
        self._flask_app_context = self._flask_app.app_context()
        self._flask_app_context.push()
        self._flask_app_client = self._flask_app.test_client()

        session = requests.Session()
        adapter = requests_mock.Adapter()
        session.mount('mock', adapter)

    def tearDown(self):
        self._db.close()
        self._flask_app_context.pop()

    @property
    def db(self):
        return Database(self._configuration['database']['url'])


class UserTest(AdministrationTestCase):
    def testUserListShould200(self):
        email = 'bart@example.com'
        user = User('Bart', 'SuperSecretPassword', email)
        user.email = email
        with self.db as db:
            db.session.add(user)
            db.session.commit()
        response = self._flask_app_client.get('/user')
        self.assertEquals(200, response.status_code)
        response_body = response.get_data(as_text=True)
        self.assertIn(email, response_body)

    @parameterized.expand([
        ('xano', 'xano@example.com', 'SuperSecretPassword', 'Xano', {Role.STAFF, Role.SYSOP}, '2063-04-05 00:00:00'),
        ('anon', 'anon@example.com', '5NeG8dBQ3DaLZv9uHJeEm53s', '', set(), '1970-01-01 00:00:00'),
    ])
    @patch('ros.auth.User.send_welcome_email')
    @patch('ros.auth.User.ensure_gitlab_user')
    @patch('ros.auth.User.ensure_rocketchat_user')
    def testUserAdd(self, name, email, password, display_name, roles,
                    expire, _send_welcome_email, _ensure_gitlab_user,
                    _ensure_rocketchat_user):
        data = MultiDict({
            'name': name,
            'email': email,
            'password': password,
            'display_name': display_name,
            'expire': expire,
        })
        data.setlist('roles', list(map(lambda r: str(r.value), roles)))
        response = self._flask_app_client.post('/user/add', data=data)
        self.assertEquals(302, response.status_code)
        with self.db as db:
            reloaded_user = db.session.query(User).order_by(User.id.desc()).first()
            self.assertEquals(reloaded_user.name, name)
            self.assertEquals(reloaded_user.email, email)
            self.assertEquals(reloaded_user.password, password)
            self.assertEquals(reloaded_user.roles, roles)
            self.assertEquals(reloaded_user.display_name, display_name)
            self.assertEquals(reloaded_user.expire, datetime.strptime(expire, '%Y-%m-%d %H:%M:%S'))

    @parameterized.expand([
        ('xano', 'xano@example.com', 'SuperSecretPassword', 'Xano', {Role.STAFF, Role.SYSOP}, '2063-04-05 00:00:00'),
        ('anon', 'anon@example.com', '5NeG8dBQ3DaLZv9uHJeEm53s', '', set(), '1970-01-01 00:00:00'),
    ])
    def testUserUpdate(self, name_updated, email_updated, password_updated, display_name_updated, roles_updated, expire_updated):
        email = 'bart@example.com'
        user = User('Bart', 'SuperSecretPassword', email)
        with self.db as db:
            db.session.add(user)
            db.session.commit()
        response = self._flask_app_client.get('/user/%d/update' % user.id)
        self.assertEquals(200, response.status_code)
        response_body = response.get_data(as_text=True)
        self.assertIn(email, response_body)

        data = MultiDict({
            'name': name_updated,
            'email': email_updated,
            'password': password_updated,
            'display_name': display_name_updated,
            'expire': expire_updated,
        })
        data.setlist('roles', list(map(lambda r: str(r.value), roles_updated)))
        response = self._flask_app_client.post('/user/%d/update' % user.id, data=data)
        self.assertEquals(302, response.status_code)
        with self.db as db:
            reloaded_user = db.session.query(User).filter(User.id == user.id).first()
            self.assertEquals(reloaded_user.name, name_updated)
            self.assertEquals(reloaded_user.email, email_updated)
            self.assertEquals(reloaded_user.password, password_updated)
            self.assertEquals(reloaded_user.roles, roles_updated)
            self.assertEquals(reloaded_user.display_name, display_name_updated)
            self.assertEquals(reloaded_user.expire, datetime.strptime(expire_updated, '%Y-%m-%d %H:%M:%S'))

    @parameterized.expand([
        ('xano', 'xano@example.com', 'Xano', {Role.STAFF, Role.SYSOP}, '2063-04-05 00:00:00'),
        ('anon', 'anon@example.com', '', set(), '1970-01-01 00:00:00'),
    ])
    def testUserUpdateWithoutPassword(self, name_updated, email_updated, display_name_updated, roles_updated, expire_updated):
        email = 'bart@example.com'
        user = User('Bart', 'SuperSecretPassword', email)
        with self.db as db:
            db.session.add(user)
            db.session.commit()
        response = self._flask_app_client.get('/user/%d/update' % user.id)
        self.assertEquals(200, response.status_code)
        response_body = response.get_data(as_text=True)
        self.assertIn(email, response_body)

        data = MultiDict({
            'name': name_updated,
            'email': email_updated,
            'password': '',
            'display_name': display_name_updated,
            'expire': expire_updated,
        })
        data.setlist('roles', list(map(lambda r: str(r.value), roles_updated)))
        response = self._flask_app_client.post('/user/%d/update' % user.id, data=data)
        self.assertEquals(302, response.status_code)
        with self.db as db:
            reloaded_user = db.session.query(User).filter(User.id == user.id).first()
            self.assertEquals(reloaded_user.name, name_updated)
            self.assertEquals(reloaded_user.email, email_updated)
            self.assertEquals(reloaded_user.roles, roles_updated)
            self.assertEquals(reloaded_user.password, 'SuperSecretPassword')
            self.assertEquals(reloaded_user.display_name, display_name_updated)
            self.assertEquals(reloaded_user.expire, datetime.strptime(expire_updated, '%Y-%m-%d %H:%M:%S'))

    def testNonExistentUserUpdateShould404(self):
        user_id = 999
        response = self._flask_app_client.get('/user/%d/update' % user_id)
        self.assertEquals(404, response.status_code)

    def testUserDelete(self):
        email = 'bart@example.com'
        user = User('Bart', 'SuperSecretPassword', email)
        with self.db as db:
            db.session.add(user)
            db.session.commit()
        response = self._flask_app_client.post('/user/%d/delete' % user.id, data={
            'submit': 'Delete',
        })
        self.assertEquals(302, response.status_code)
        with self.db as db:
            reloaded_user = db.session.query(User).filter(User.id == user.id).first()
            self.assertIsNone(reloaded_user)

    def testNonExistentUserDeleteShould404(self):
        user_id = 999
        response = self._flask_app_client.get('/user/%d/update' % user_id)
        self.assertEquals(404, response.status_code)


class ProjectTest(AdministrationTestCase):
    def testProjectListShould200(self):
        name = 'Good stuff!'
        project = Project(ProjectType.QUOTE, name)
        with self.db as db:
            db.session.add(project)
            db.session.commit()
        response = self._flask_app_client.get('/project')
        self.assertEquals(200, response.status_code)
        response_body = response.get_data(as_text=True)
        self.assertIn(name, response_body)

    def testProjectViewShould200(self):
        name = 'Good stuff!'
        project = Project(ProjectType.QUOTE, name)
        with self.db as db:
            db.session.add(project)
            db.session.commit()
        response = self._flask_app_client.get('/project/%d' % project.id)
        self.assertEquals(200, response.status_code)
        response_body = response.get_data(as_text=True)
        self.assertIn(project.chat_url, response_body)
        self.assertIn(project.repository_url, response_body)

    @parameterized.expand([
        (ProjectType.PENTEST, 'Ballpoint', 'pen-ballpoint', 'RockyRoad777', 'pen-ballpoint', 123),
        (ProjectType.QUOTE, 'Kwoot', '', '', '', None),
    ])
    def testProjectAdd(self, project_type, name, rocketchat_room_name, rocketchat_id, gitlab_repository_name, gitlab_id):
        data = {
            'type': project_type.value,
            'name': name,
            'rocketchat_room_name': rocketchat_room_name,
            'rocketchat_id': rocketchat_id,
            'gitlab_repository_name': gitlab_repository_name,
            'gitlab_id': gitlab_id,
        }
        response = self._flask_app_client.post('/project/add', data=data)
        self.assertEquals(302, response.status_code)
        with self.db as db:
            reloaded_project = db.session.query(Project).order_by(Project.id.desc()).first()
            self.assertEquals(reloaded_project.name, name)
            self.assertEquals(reloaded_project.rocketchat_id, rocketchat_id)
            self.assertEquals(reloaded_project.gitlab_id, gitlab_id)

    @parameterized.expand([
        ('Kwoot-final', 'off-kwoot', 'RockyRoad777', 'off-kwoot', 123),
        ('Kwoot-final3', '', '', '', None),
    ])
    def testProjectUpdate(self, name_updated, rocketchat_room_name_updated, rocketchat_id_updated, gitlab_repository_name_updated, gitlab_id_updated):
        project_type = ProjectType.QUOTE
        name = 'Kwoot'
        project = Project(project_type, name)
        with self.db as db:
            db.session.add(project)
            db.session.commit()
        response = self._flask_app_client.get('/project/%d/update' % project.id)
        self.assertEquals(200, response.status_code)
        response_body = response.get_data(as_text=True)
        self.assertIn(name, response_body)

        data = {
            'name': name_updated,
            'rocketchat_room_name': rocketchat_room_name_updated,
            'rocketchat_id': rocketchat_id_updated,
            'gitlab_repository_name': gitlab_repository_name_updated,
            'gitlab_id': gitlab_id_updated,
        }
        response = self._flask_app_client.post('/project/%d/update' % project.id, data=data)
        self.assertEquals(302, response.status_code)
        with self.db as db:
            reloaded_project = db.session.query(Project).filter(Project.id == project.id).first()
            self.assertEquals(reloaded_project.type, project_type)
            self.assertEquals(reloaded_project.name, name_updated)
            self.assertEquals(reloaded_project.rocketchat_room_name, rocketchat_room_name_updated)
            self.assertEquals(reloaded_project.rocketchat_id, rocketchat_id_updated)
            self.assertEquals(reloaded_project.gitlab_repository_name, gitlab_repository_name_updated)
            self.assertEquals(reloaded_project.gitlab_id, gitlab_id_updated)

    def testNonExistentProjectUpdateShould404(self):
        project_id = 999
        response = self._flask_app_client.get('/project/%d/update' % project_id)
        self.assertEquals(404, response.status_code)

    def testProjectDelete(self):
        project = Project(ProjectType.QUOTE, 'Kwoot')
        with self.db as db:
            db.session.add(project)
            db.session.commit()
        response = self._flask_app_client.post('/project/%d/delete' % project.id, data={
            'submit': 'Delete',
        })
        self.assertEquals(302, response.status_code)
        with self.db as db:
            reloaded_project = db.session.query(Project).filter(Project.id == project.id).first()
            self.assertIsNone(reloaded_project)

    def testNonExistentProjectDeleteShould404(self):
        project_id = 999
        response = self._flask_app_client.get('/project/%d/update' % project_id)
        self.assertEquals(404, response.status_code)
