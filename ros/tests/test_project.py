from datetime import datetime, date
from unittest import TestCase

import requests_mock
from parameterized import parameterized

from ros.db import TemporaryDatabase
from ros.project import Project, ProjectType


class ProjectTest(TestCase):
    def test_new(self):
        project_type = ProjectType.QUOTE
        name = 'make-coffee'
        sut = Project(project_type, name)
        self.assertIsInstance(sut, Project)
        self.assertEquals(sut.type, project_type)
        self.assertEquals(sut.name, name)
        self.assertIsInstance(sut.created, datetime)

    def test_serialize_minimal(self):
        project_id = 313
        project_name = 'Salo'
        project = Project(ProjectType.SALES, project_name)
        project.id = project_id

        expected = {
            'id': project_id,
            'type': ProjectType.SALES.name,
            'name': project_name,
            'rocketchat-room-name': None,
        }
        actual = project.serialize()
        self.assertEqual(actual, expected)

    def test_serialize_full(self):
        project_id = 313
        project_name = 'Salo'
        project_rocketchat_room_name = 'three-one-three'
        project = Project(ProjectType.SALES, project_name)
        project.id = project_id
        project.rocketchat_room_name = project_rocketchat_room_name

        expected = {
            'id': project_id,
            'type': ProjectType.SALES.name,
            'name': project_name,
            'rocketchat-room-name': project_rocketchat_room_name,
        }
        actual = project.serialize()
        self.assertEqual(actual, expected)

    def test_chat_url(self):
        project_type = ProjectType.QUOTE
        name = 'Make coffee'
        rocketchat_room_name = 'make-coffee'
        sut = Project(project_type, name)
        sut.rocketchat_room_name = rocketchat_room_name
        self.assertEquals(sut.chat_url, 'https://chat.radicallyopensecurity.com/group/%s' % rocketchat_room_name)

    def test_repository_url(self):
        project_type = ProjectType.QUOTE
        name = 'Make coffee'
        gitlab_repository_name = 'make-coffee'
        sut = Project(project_type, name)
        sut.gitlab_repository_name = gitlab_repository_name
        self.assertEquals(sut.repository_url,
                          'https://gitlabs.radicallyopensecurity.com/ros/%s' % gitlab_repository_name)

    def _prepare_report(self, m_requests, start_value='', end_value='', persondays_value=''):
        project = Project(ProjectType.PENTEST, 'Fountain')
        project.gitlab_repository_name = 'fountain'
        xml = '''<pentest_report>
                        <meta>
                            <activityinfo>
                                <persondays>{persondays_value}</persondays>
                                <planning>
                                    <start>{start_value}</start>
                                    <end>{end_value}</end>
                                </planning>
                            </activityinfo>
                        </meta>
                </pentest_report>'''.format(persondays_value=persondays_value, start_value=start_value,
                                            end_value=end_value)
        url = 'https://gitlabs.radicallyopensecurity.com/ros/fountain/raw/master/source/report.xml'
        m_requests.get(url, text=xml, status_code=200)
        return project

    @parameterized.expand([
        ('', None),
        ('1978-12-07', date(1978, 12, 7),),
    ])
    @requests_mock.Mocker()
    def test_start(self, start_value, expected, m_requests):
        project = self._prepare_report(m_requests, start_value=start_value)
        self.assertEqual(project.start, expected)

    @requests_mock.Mocker()
    def test_start_with_invalid_value(self, m_requests):
        project = self._prepare_report(m_requests, start_value='0000-00-00')
        with self.assertRaises(ValueError):
            project.start

    @parameterized.expand([
        ('', None),
        ('1978-12-07', date(1978, 12, 7),),
    ])
    @requests_mock.Mocker()
    def test_end(self, end_value, expected, m_requests):
        project = self._prepare_report(m_requests, end_value=end_value)
        self.assertEqual(project.end, expected)

    @requests_mock.Mocker()
    def test_end_with_invalid_value(self, m_requests):
        project = self._prepare_report(m_requests, end_value='0000-00-00')
        with self.assertRaises(ValueError):
            project.end

    @parameterized.expand([
        ('', None),
        ('5', 40),
        ('8.7', 69.6),
    ])
    @requests_mock.Mocker()
    def test_budgeted_hours(self, persondays_value, expected, m_requests):
        project = self._prepare_report(m_requests, persondays_value=persondays_value)
        self.assertEqual(project.budgeted_hours, expected)

    @requests_mock.Mocker()
    def test_budgeted_hours_with_invalid_value(self, m_requests):
        project = self._prepare_report(m_requests, persondays_value='zero-day')
        with self.assertRaises(ValueError):
            project.budgeted_hours

    def test_crud(self):
        with TemporaryDatabase() as db:
            project = Project(ProjectType.QUOTE, 'make-coffee')
            db.session.add(project)
            db.session.commit()
            loaded_project = db.session.query(Project).get(project.id)
            self.assertEquals(loaded_project, project)
