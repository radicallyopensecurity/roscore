from datetime import datetime
from unittest import TestCase

from ros.db import TemporaryDatabase
from ros.mailsystem import Project, ProjectType


class ProjectTest(TestCase):
    def test_new(self):
        project_type = ProjectType.pet
        name = 'testproject'
        short_name = 'tp'
        number = '2020-01-01'
        applicant = 'testapplicant'
        email = 'test@radicallyopensecurity.com'
        welcome_email_sent = False
        last_activity = datetime.max
        sut = Project(name=name, short_name=short_name, number=number, applicant=applicant, email=email, type=project_type,
                      welcome_email_sent=welcome_email_sent, last_activity=last_activity)
        self.assertIsInstance(sut, Project)
        self.assertEquals(sut.type, project_type)
        self.assertEquals(sut.name, name)
        self.assertEquals(sut.short_name, short_name)
        self.assertEquals(sut.number, number)
        self.assertEquals(sut.email, email)
        self.assertEquals(sut.welcome_email_sent, welcome_email_sent)
        self.assertEquals(sut.last_activity, last_activity)

    def test_serialize_full(self):
        project_id = 123
        project_type = ProjectType.pet
        name = 'testproject'
        short_name = 'tp'
        number = '2020-01-01'
        applicant = 'testapplicant'
        email = 'test@radicallyopensecurity.com'
        welcome_email_sent = False
        last_activity = datetime.max
        project = Project(name=name, short_name=short_name, number=number, applicant=applicant, email=email, type=project_type,
                          welcome_email_sent=welcome_email_sent, last_activity=last_activity)
        project.id = project_id

        expected = {
            'id': project_id,
            'name': name,
            'short_name': short_name,
            'number': number,
            'applicant': applicant,
            'email': email,
            'welcome_email_sent': welcome_email_sent,
            'last_activity': last_activity,
            'type': ProjectType.pet,
        }
        actual = project.serialize()
        self.assertEqual(actual, expected)

    def test_call(self):
        project_type = ProjectType.pet
        name = 'testproject'
        short_name = 'tp'
        number = '2020-01-01'
        applicant = 'testapplicant'
        email = 'test@radicallyopensecurity.com'
        welcome_email_sent = False
        last_activity = datetime.min
        sut = Project(name=name, short_name=short_name, number=number, applicant=applicant, email=email, type=project_type,
                      welcome_email_sent=welcome_email_sent, last_activity=last_activity)
        self.assertEquals(sut.call, '2020-01P')

    def test_crud(self):
        with TemporaryDatabase() as db:
            project_type = ProjectType.pet
            name = 'testproject'
            short_name = 'tp'
            number = '2020-01-01'
            applicant = 'testapplicant'
            email = 'test@radicallyopensecurity.com'
            welcome_email_sent = False
            last_activity = datetime.min
            project = Project(name=name, short_name=short_name, number=number, applicant=applicant, email=email,
                              type=project_type, welcome_email_sent=welcome_email_sent, last_activity=last_activity)
            db.session.add(project)
            db.session.commit()
            loaded_project = db.session.query(Project).get(project.id)
            self.assertEquals(loaded_project, project)
