import argparse
import json
import os
from datetime import datetime
from unittest import TestCase

from tempfile import TemporaryDirectory
from unittest.mock import patch

from parameterized import parameterized

import ros
from ros.cli.args import add_configuration_to_args, parse_configuration_from_args, add_messenger_to_args, \
    parse_messenger_from_args, date
from ros.config import TemporaryConfiguration
from ros.messenger import Messenger


class ConfigurationTest(TestCase):
    def test_with_file_path_should_parse(self):
        expected_configuration = {
            'database': {
                'url': 'ros://db',
            },
            'flask': {},
            'gitlab': {
                'url': 'https://example.com',
                'token': 'foo',
                'namespace': 'bar',
                'user': {
                    'name': 'ros',
                    'email': 'ros@example.com',
                },
            },
            'rocketchat': {
                'url': 'https://example.com',
                'user': 'test',
                'password': 'test pass',
            },
            'smtp-server': 'mail',
        }
        with TemporaryConfiguration(expected_configuration) as f:
            parser = argparse.ArgumentParser()
            add_configuration_to_args(parser)
            args = ['--configuration-path', f.name]
            cli_args = parser.parse_args(args)
            actual_configuration = parse_configuration_from_args(cli_args)
            self.assertEquals(actual_configuration, expected_configuration)

    def test_with_directory_path_should_parse(self):
        configuration_1 = {
            'database': {
                'url': 'notros://nodb',
            },
            'flask': {
                'DEBUG': False,
            },
            'gitlab': {
                'url': 'https://example.com',
                'token': 'foo',
                'namespace': 'bar',
                'user': {
                    'name': 'ros',
                    'email': 'ros@example.com',
                },
            },
            'rocketchat': {
                'url': 'https://example.com',
                'user': 'test',
                'password': 'test pass',
            },
            'smtp-server': 'mail',

        }
        configuration_2 = {
            'database': {
                'url': 'ros://db',
            },
            'flask': {
                'DEBUG': True,
            },
            'gitlab': {
                'url': 'https://example.com',
                'token': 'foo',
                'namespace': 'bar',
                'user': {
                    'name': 'ros',
                    'email': 'ros@example.com',
                },
            },
            'rocketchat': {
                'url': 'https://example.com',
                'user': 'test',
                'password': 'test pass',
            },
            'smtp-server': 'mail',
        }
        with TemporaryDirectory() as configuration_path:
            with open(os.path.join(configuration_path, '1.json'), mode='w+t') as f_1:
                with open(os.path.join(configuration_path, '2.json'), mode='w+t') as f_2:
                    parser = argparse.ArgumentParser()
                    add_configuration_to_args(parser)
                    args = ['--configuration-path', configuration_path]

                    json.dump(configuration_1, f_1)
                    f_1.seek(0)
                    json.dump(configuration_2, f_2)
                    f_2.seek(0)
                    cli_args = parser.parse_args(args)
                    actual_configuration = parse_configuration_from_args(cli_args)
                    self.assertEquals(actual_configuration, configuration_2)

                    json.dump(configuration_1, f_2)
                    f_1.seek(0)
                    json.dump(configuration_2, f_1)
                    f_2.seek(0)
                    cli_args = parser.parse_args(args)
                    actual_configuration = parse_configuration_from_args(cli_args)
                    self.assertEquals(actual_configuration, configuration_1)


class MessengerTest(TestCase):
    @patch('ros.messenger.RosbotMessenger')
    def test_messenger(self, m_messenger):
        address = 'https://example.com/rocketchat'
        room_name = 'woof'
        configuration = {
            'rocketchat': {
                'url': address,
                'user': 'test user',
                'password': 'test pass',
            },
        }
        parser = argparse.ArgumentParser()
        add_messenger_to_args(parser)
        args = parser.parse_args(['--messenger-rocketchat-room', room_name])
        messenger = parse_messenger_from_args(configuration, args)
        self.assertIsInstance(messenger, Messenger)
        ros.messenger.RosbotMessenger.assert_called_with(address, room_name)

    @patch('ros.messenger.RosbotMessenger')
    def test_messenger_without_configuration(self, m_messenger):
        configuration = {}
        parser = argparse.ArgumentParser()
        add_messenger_to_args(parser)
        args = parser.parse_args(['--messenger-rocketchat-room', 'foo'])
        messenger = parse_messenger_from_args(configuration, args)
        self.assertIsInstance(messenger, Messenger)
        ros.messenger.RosbotMessenger.assert_not_called()

    @patch('ros.messenger.RosbotMessenger')
    def test_messenger_without_room(self, m_messenger):
        configuration = {
            'rocketchat': {
                'url': 'https://example.com/rocketchat',
                'user': 'test user',
                'password': 'test pass',
            },
        }
        parser = argparse.ArgumentParser()
        add_messenger_to_args(parser)
        args = parser.parse_args([])
        messenger = parse_messenger_from_args(configuration, args)
        self.assertIsInstance(messenger, Messenger)
        ros.messenger.RosbotMessenger.assert_not_called()


class DateTest(TestCase):
    @parameterized.expand([
        ('1978-12-07', datetime(1978, 12, 7)),
    ])
    def test_valid_date(self, value, expected):
        self.assertEquals(date(value), expected)

    @parameterized.expand([
        ('07-12-1978',),
        ('07-12-0000',),
    ])
    def test_should_raise_error(self, value):
        with self.assertRaises(ValueError):
            date(value)
