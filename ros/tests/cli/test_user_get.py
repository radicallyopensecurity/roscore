import contextlib
import io
import json
from unittest import TestCase

from ros.auth import User
from ros.cli.user_get import main
from ros.config import TemporaryConfiguration
from ros.db import TemporaryDatabase


class MainTest(TestCase):
    def test_without_users(self):
        with TemporaryDatabase() as db:
            configuration = {
                'database': {
                    'url': db.url,
                },
                'flask': {},
                'gitlab': {
                    'url': 'https://example.com',
                    'token': 'foo',
                    'namespace': 'bar',
                    'user': {
                        'name': 'ros',
                        'email': 'ros@example.com',
                    },
                },
                'rocketchat': {
                    'url': 'https://example.com',
                    'user': 'test',
                    'password': 'test pass',
                }
            }
            with TemporaryConfiguration(configuration) as f:
                args = ['--configuration-path', f.name]
                stdout = io.StringIO()
                with contextlib.redirect_stdout(stdout):
                    main(args)

        expected_users = []
        actual_users = json.loads(stdout.getvalue())
        self.assertEqual(actual_users, expected_users)

    def test_without_filters(self):
        user_1_id = 313
        user_1_name = 'Hans'
        user_1_email = 'hans@example.com'
        user_1 = User(user_1_name, 'Numbers', user_1_email)
        user_1.id = user_1_id

        user_2_id = 666
        user_2_name = 'Heinz'
        user_2_email = 'heinz@example.com'
        user_2 = User(user_2_name, 'Ketchup', user_2_email)
        user_2.id = user_2_id

        with TemporaryDatabase() as db:
            db.session.add(user_1)
            db.session.add(user_2)
            db.session.commit()

            configuration = {
                'database': {
                    'url': db.url,
                },
                'flask': {},
                'gitlab': {
                    'url': 'https://example.com',
                    'token': 'foo',
                    'namespace': 'bar',
                    'user': {
                        'name': 'ros',
                        'email': 'ros@example.com',
                    },
                },
                'rocketchat': {
                    'url': 'https://example.com',
                    'user': 'test',
                    'password': 'test pass',
                }
            }
            with TemporaryConfiguration(configuration) as f:
                args = ['--configuration-path', f.name]
                stdout = io.StringIO()
                with contextlib.redirect_stdout(stdout):
                    main(args)

        expected_users = [
            {
                'id': user_1_id,
                'name': user_1_name,
                'email': user_1_email,
                'rocketchat-id': None,
                'gitlab-id': None,
                'roles': [],
            },
            {
                'id': user_2_id,
                'name': user_2_name,
                'email': user_2_email,
                'rocketchat-id': None,
                'gitlab-id': None,
                'roles': [],
            },
        ]
        actual_users = json.loads(stdout.getvalue())
        self.assertEqual(actual_users, expected_users)

    def test_with_rocketchat_id(self):
        user_1_id = 313
        user_1_name = 'Hans'
        user_1_email = 'hans@example.com'
        user_1_rocketchat_id = '313'
        user_1 = User(user_1_name, 'Numbers', user_1_email)
        user_1.id = user_1_id
        user_1.rocketchat_id = user_1_rocketchat_id

        user_2_id = 666
        user_2_name = 'Heinz'
        user_2_email = 'heinz@example.com'
        user_2 = User(user_2_name, 'Ketchup', user_2_email)
        user_2.id = user_2_id

        with TemporaryDatabase() as db:
            db.session.add(user_1)
            db.session.add(user_2)
            db.session.commit()

            configuration = {
                'database': {
                    'url': db.url,
                },
                'flask': {},
                'gitlab': {
                    'url': 'https://example.com',
                    'token': 'foo',
                    'namespace': 'bar',
                    'user': {
                        'name': 'ros',
                        'email': 'ros@example.com',
                    },
                },
                'rocketchat': {
                    'url': 'https://example.com',
                    'user': 'test',
                    'password': 'test pass',
                }
            }
            with TemporaryConfiguration(configuration) as f:
                args = ['--configuration-path', f.name, '--rocketchat-id', user_1_rocketchat_id]
                stdout = io.StringIO()
                with contextlib.redirect_stdout(stdout):
                    main(args)

        expected_users = [
            {
                'id': user_1_id,
                'name': user_1_name,
                'email': user_1_email,
                'rocketchat-id': user_1_rocketchat_id,
                'gitlab-id': None,
                'roles': [],
            },
        ]
        actual_users = json.loads(stdout.getvalue())
        self.assertEqual(actual_users, expected_users)
