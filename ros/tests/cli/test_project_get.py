import contextlib
import io
import json
from unittest import TestCase

from ros.cli.project_get import main
from ros.config import TemporaryConfiguration
from ros.db import TemporaryDatabase
from ros.project import Project, ProjectType


class MainTest(TestCase):
    def test_without_projects(self):
        with TemporaryDatabase() as db:
            configuration = {
                'database': {
                    'url': db.url,
                },
                'flask': {},
                'gitlab': {
                    'url': 'https://example.com',
                    'token': 'foo',
                    'namespace': 'bar',
                    'user': {
                        'name': 'ros',
                        'email': 'ros@example.com',
                    },
                },
                'rocketchat': {
                    'url': 'https://example.com',
                    'user': 'test',
                    'password': 'test pass',
                }
            }
            with TemporaryConfiguration(configuration) as f:
                args = ['--configuration-path', f.name]
                stdout = io.StringIO()
                with contextlib.redirect_stdout(stdout):
                    main(args)

        expected_projects = []
        actual_projects = json.loads(stdout.getvalue())
        self.assertEqual(actual_projects, expected_projects)

    def test_without_filters(self):
        project_1_id = 313
        project_1_name = 'Salo'
        project_1_rocketchat_room_name = 'three-one-three'
        project_1 = Project(ProjectType.SALES, project_1_name)
        project_1.id = project_1_id
        project_1.rocketchat_room_name = project_1_rocketchat_room_name

        project_2_id = 666
        project_2_name = 'Z Mayonezom'
        project_2 = Project(ProjectType.QUOTE, project_2_name)
        project_2.id = project_2_id

        with TemporaryDatabase() as db:
            db.session.add(project_1)
            db.session.add(project_2)
            db.session.commit()

            configuration = {
                'database': {
                    'url': db.url,
                },
                'flask': {},
                'gitlab': {
                    'url': 'https://example.com',
                    'token': 'foo',
                    'namespace': 'bar',
                    'user': {
                        'name': 'ros',
                        'email': 'ros@example.com',
                    },
                },
                'rocketchat': {
                    'url': 'https://example.com',
                    'user': 'test',
                    'password': 'test pass',
                }
            }
            with TemporaryConfiguration(configuration) as f:
                args = ['--configuration-path', f.name]
                stdout = io.StringIO()
                with contextlib.redirect_stdout(stdout):
                    main(args)

        expected_projects = [
            {
                'id': project_1_id,
                'type': ProjectType.SALES.name,
                'name': project_1_name,
                'rocketchat-room-name': project_1_rocketchat_room_name,
            },
            {
                'id': project_2_id,
                'type': ProjectType.QUOTE.name,
                'name': project_2_name,
                'rocketchat-room-name': None,
            },
        ]
        actual_projects = json.loads(stdout.getvalue())
        self.assertEqual(actual_projects, expected_projects)

    def test_with_rocketchat_room(self):
        project_1_id = 313
        project_1_name = 'Salo'
        project_1_rocketchat_room_name = 'three-one-three'
        project_1 = Project(ProjectType.SALES, project_1_name)
        project_1.id = project_1_id
        project_1.rocketchat_room_name = project_1_rocketchat_room_name

        project_2_id = 666
        project_2_name = 'Z Mayonezom'
        project_2 = Project(ProjectType.QUOTE, project_2_name)
        project_2.id = project_2_id

        with TemporaryDatabase() as db:
            db.session.add(project_1)
            db.session.add(project_2)
            db.session.commit()

            configuration = {
                'database': {
                    'url': db.url,
                },
                'flask': {},
                'gitlab': {
                    'url': 'https://example.com',
                    'token': 'foo',
                    'namespace': 'bar',
                    'user': {
                        'name': 'ros',
                        'email': 'ros@example.com',
                    },
                },
                'rocketchat': {
                    'url': 'https://example.com',
                    'user': 'test',
                    'password': 'test pass',
                }
            }
            with TemporaryConfiguration(configuration) as f:
                args = ['--configuration-path', f.name, '--rocketchat-room', project_1_rocketchat_room_name]
                stdout = io.StringIO()
                with contextlib.redirect_stdout(stdout):
                    main(args)

        expected_projects = [
            {
                'id': project_1_id,
                'type': ProjectType.SALES.name,
                'name': project_1_name,
                'rocketchat-room-name': project_1_rocketchat_room_name,
            },
        ]
        actual_projects = json.loads(stdout.getvalue())
        self.assertEqual(actual_projects, expected_projects)
