from unittest import TestCase
from unittest.mock import patch

from parameterized import parameterized

from ros.auth import User
from ros.charge import Charge
from ros.cli.charge import main
from ros.config import TemporaryConfiguration
from ros.db import TemporaryDatabase
from ros.project import Project, ProjectType


class ChargeTest(TestCase):
    @parameterized.expand(
        [
            ("2018-10-25", 7, "private"),
            ("2018-10-25", 7, "public"),
            ("1970-01-01", 8.9, "private"),
            ("1970-01-01", 8.9, "public"),
        ]
    )
    @patch("sys.stderr")
    def test(self, date, hours, source, m_stderr):
        with TemporaryDatabase() as db:
            user = User("Hans", "Numbers", "hans@example.com")
            project = Project(ProjectType.SALES, "Salo")
            db.session.add(user)
            db.session.add(project)
            db.session.commit()

            configuration = {
                "database": {
                    "url": db.url,
                },
                "flask": {},
                "gitlab": {
                    "url": "https://example.com",
                    "token": "foo",
                    "namespace": "bar",
                    "user": {
                        "name": "ros",
                        "email": "ros@example.com",
                    },
                },
                "rocketchat": {
                    "url": "https://example.com",
                    "user": "test",
                    "password": "test pass",
                },
            }
            with TemporaryConfiguration(configuration) as f:
                args = [
                    "--configuration-path",
                    f.name,
                    "--user-id",
                    str(user.id),
                    "--project-id",
                    str(project.id),
                    "--date",
                    date,
                    "--hours",
                    str(hours),
                    "--source",
                    str(source),
                ]
                main(args)
            charge = db.session.query(Charge).first()
            self.assertEqual(charge.date.strftime("%Y-%m-%d"), date)
            self.assertEqual(charge.hours, hours)

    @patch("sys.stderr")
    def test_with_non_existent_user(self, m_stderr):
        with TemporaryDatabase() as db:
            project = Project(ProjectType.SALES, "Salo")
            db.session.add(project)
            db.session.commit()

            configuration = {
                "database": {
                    "url": db.url,
                },
                "flask": {},
                "gitlab": {
                    "url": "https://example.com",
                    "token": "foo",
                    "namespace": "bar",
                    "user": {
                        "name": "ros",
                        "email": "ros@example.com",
                    },
                },
                "rocketchat": {
                    "url": "https://example.com",
                    "user": "test",
                    "password": "test pass",
                },
            }
            date = "2018-10-25"
            hours = 7
            source = "public"
            with TemporaryConfiguration(configuration) as f:
                args = [
                    "--configuration-path",
                    f.name,
                    "--user-id",
                    "7",
                    "--project-id",
                    str(project.id),
                    "--date",
                    date,
                    "--hours",
                    str(hours),
                    "--source",
                    str(source),
                ]
                with self.assertRaises(ValueError):
                    main(args)

    @patch("sys.stderr")
    def test_with_non_existent_project(self, m_stderr):
        with TemporaryDatabase() as db:
            user = User("Hans", "Numbers", "hans@example.com")
            db.session.add(user)
            db.session.commit()

            configuration = {
                "database": {
                    "url": db.url,
                },
                "flask": {},
                "gitlab": {
                    "url": "https://example.com",
                    "token": "foo",
                    "namespace": "bar",
                    "user": {
                        "name": "ros",
                        "email": "ros@example.com",
                    },
                },
                "rocketchat": {
                    "url": "https://example.com",
                    "user": "test",
                    "password": "test pass",
                },
            }
            date = "2018-10-25"
            hours = 7
            source = "private"
            with TemporaryConfiguration(configuration) as f:
                args = [
                    "--configuration-path",
                    f.name,
                    "--user-id",
                    str(user.id),
                    "--project-id",
                    "7",
                    "--date",
                    date,
                    "--hours",
                    str(hours),
                    "--source",
                    str(source),
                ]
                with self.assertRaises(ValueError):
                    main(args)

    @patch("sys.stderr")
    def test_with_non_existent_rocketchat_roomname(self, m_stderr):
        with TemporaryDatabase() as db:
            user = User("Hans", "Numbers", "hans@example.com")
            db.session.add(user)
            db.session.commit()

            configuration = {
                "database": {
                    "url": db.url,
                },
                "flask": {},
                "gitlab": {
                    "url": "https://example.com",
                    "token": "foo",
                    "namespace": "bar",
                    "user": {
                        "name": "ros",
                        "email": "ros@example.com",
                    },
                },
                "rocketchat": {
                    "url": "https://example.com",
                    "user": "test",
                    "password": "test pass",
                },
            }
            date = "2018-10-25"
            hours = 7
            source = "private"
            with TemporaryConfiguration(configuration) as f:
                args = [
                    "--configuration-path",
                    f.name,
                    "--user-id",
                    str(user.id),
                    "--rocketchat-room-name",
                    "pen-doesnotexist",
                    "--date",
                    date,
                    "--hours",
                    str(hours),
                    "--source",
                    str(source),
                ]
                with self.assertRaises(ValueError):
                    main(args)

    @patch("sys.stderr")
    def test_with_non_existent_rocketchat_user_id(self, m_stderr):
        with TemporaryDatabase() as db:
            project = Project(ProjectType.SALES, "Salo")
            db.session.add(project)
            db.session.commit()

            configuration = {
                "database": {
                    "url": db.url,
                },
                "flask": {},
                "gitlab": {
                    "url": "https://example.com",
                    "token": "foo",
                    "namespace": "bar",
                    "user": {
                        "name": "ros",
                        "email": "ros@example.com",
                    },
                },
                "rocketchat": {
                    "url": "https://example.com",
                    "user": "test",
                    "password": "test pass",
                },
            }
            date = "2018-10-25"
            hours = 7
            source = "private"
            with TemporaryConfiguration(configuration) as f:
                args = [
                    "--configuration-path",
                    f.name,
                    "--rocketchat-user-id",
                    "nonexistinguser",
                    "--project-id",
                    str(project.id),
                    "--date",
                    date,
                    "--hours",
                    str(hours),
                    "--source",
                    str(source),
                ]
                with self.assertRaises(ValueError):
                    main(args)

    @patch("sys.stderr")
    def test_with_invalid_date(self, m_stderr):
        with TemporaryDatabase() as db:
            user = User("Hans", "Numbers", "hans@example.com")
            project = Project(ProjectType.SALES, "Salo")
            db.session.add(user)
            db.session.add(project)
            db.session.commit()

            configuration = {
                "database": {
                    "url": db.url,
                },
                "flask": {},
                "gitlab": {
                    "url": "https://example.com",
                    "token": "foo",
                    "namespace": "bar",
                    "user": {
                        "name": "ros",
                        "email": "ros@example.com",
                    },
                },
                "rocketchat": {
                    "url": "https://example.com",
                    "user": "test",
                    "password": "test pass",
                },
            }
            date = "0000-10-25"
            hours = 7
            source = "public"
            with TemporaryConfiguration(configuration) as f:
                args = [
                    "--configuration-path",
                    f.name,
                    "--user-id",
                    str(user.id),
                    "--project-id",
                    str(project.id),
                    "--date",
                    date,
                    "--hours",
                    str(hours),
                    "--source",
                    str(source),
                ]
                with self.assertRaises(SystemExit):
                    main(args)

    @patch("sys.stderr")
    def test_with_invalid_hours(self, m_stderr):
        with TemporaryDatabase() as db:
            user = User("Hans", "Numbers", "hans@example.com")
            project = Project(ProjectType.SALES, "Salo")
            db.session.add(user)
            db.session.add(project)
            db.session.commit()

            configuration = {
                "database": {
                    "url": db.url,
                },
                "flask": {},
                "gitlab": {
                    "url": "https://example.com",
                    "token": "foo",
                    "namespace": "bar",
                    "user": {
                        "name": "ros",
                        "email": "ros@example.com",
                    },
                },
                "rocketchat": {
                    "url": "https://example.com",
                    "user": "test",
                    "password": "test pass",
                },
            }
            date = "2018-10-25"
            hours = "seven"
            source = "private"
            with TemporaryConfiguration(configuration) as f:
                args = [
                    "--configuration-path",
                    f.name,
                    "--user-id",
                    str(user.id),
                    "--project-id",
                    str(project.id),
                    "--date",
                    date,
                    "--hours",
                    hours,
                    "--source",
                    str(source),
                ]
                with self.assertRaises(SystemExit):
                    main(args)

    @patch("sys.stderr")
    def test_with_user_id_and_rc_name(self, m_stderr):
        with TemporaryDatabase() as db:
            user = User("Hans", "Numbers", "hans@example.com")
            project = Project(ProjectType.SALES, "Salo")
            db.session.add(user)
            db.session.add(project)
            db.session.commit()

            configuration = {
                "database": {
                    "url": db.url,
                },
                "flask": {},
                "gitlab": {
                    "url": "https://example.com",
                    "token": "foo",
                    "namespace": "bar",
                    "user": {
                        "name": "ros",
                        "email": "ros@example.com",
                    },
                },
                "rocketchat": {
                    "url": "https://example.com",
                    "user": "test",
                    "password": "test pass",
                },
            }
            date = "2020-10-25"
            hours = 7
            source = "public"
            rocketchatid = "R2D2"
            with TemporaryConfiguration(configuration) as f:
                args = [
                    "--configuration-path",
                    f.name,
                    "--user-id",
                    str(user.id),
                    "--rocketchat-user-id",
                    str(rocketchatid),
                    "--project-id",
                    str(project.id),
                    "--date",
                    date,
                    "--hours",
                    str(hours),
                    "--source",
                    str(source),
                ]
                with self.assertRaises(SystemExit):
                    main(args)

    @patch("sys.stderr")
    def test_with_project_id_and_rc_roomname(self, m_stderr):
        with TemporaryDatabase() as db:
            user = User("Hans", "Numbers", "hans@example.com")
            project = Project(ProjectType.SALES, "Salo")
            db.session.add(user)
            db.session.add(project)
            db.session.commit()

            configuration = {
                "database": {
                    "url": db.url,
                },
                "flask": {},
                "gitlab": {
                    "url": "https://example.com",
                    "token": "foo",
                    "namespace": "bar",
                    "user": {
                        "name": "ros",
                        "email": "ros@example.com",
                    },
                },
                "rocketchat": {
                    "url": "https://example.com",
                    "user": "test",
                    "password": "test pass",
                },
            }
            date = "2020-10-25"
            hours = 7
            source = "public"
            rocketchatroomname = "pen-test"
            with TemporaryConfiguration(configuration) as f:
                args = [
                    "--configuration-path",
                    f.name,
                    "--user-id",
                    str(user.id),
                    "--rocketchat-room-name",
                    str(rocketchatroomname),
                    "--project-id",
                    str(project.id),
                    "--date",
                    date,
                    "--hours",
                    str(hours),
                    "--source",
                    str(source),
                ]
                with self.assertRaises(SystemExit):
                    main(args)

    # def test_get_charged_total(self):
    #     charge = Charge(self.user_id, self.project_id, self.date,
    #                     self.hours)
    #     self.session.add(charge)
    #     self.session.commit()
    #     self.assertIsInstance(get_charged_total(
    #         self.project_id, self._db), float)
    #     self.assertTrue(get_charged_total(self.project_id, self._db) > 0)

    # def test_get_charged_per_pentester(self):
    #     charge1 = Charge(2, self.project_id, self.date,
    #                      self.hours)
    #     charge2 = Charge(2, self.project_id, self.date,
    #                      self.hours)
    #     charge3 = Charge(2, 999, self.date,
    #                      self.hours)
    #     self.session.add(charge1)
    #     self.session.add(charge2)
    #     self.session.add(charge3)
    #     self.session.commit()
    #     self.assertIsInstance(get_charged_per_pentester(
    #         self.project_id, 2, self._db), float)
    #     self.assertEquals(get_charged_per_pentester(
    #         self.project_id, 2, self._db), 3)

    # def test_warning_level(self):
    #     self.assertTrue(warning_level(100, 10, 55))
    #     self.assertTrue(warning_level(100, 10, 60))
    #     self.assertFalse(warning_level(100, 10, 61))
    #     self.assertEqual(warning_level(100, 1, 51), 50)
    #     self.assertEqual(warning_level(100, 1, 76), 75)
    #     self.assertEqual(warning_level(100, 1, 91), 90)
    #     self.assertEqual(warning_level(100, 1, 101), 100)
    #     self.assertEqual(warning_level(100, 1, 99), 0)
    #     self.assertEqual(warning_level(100, 85, 95), 90)

    # def test_warn(self):
    #     tested_warning = warn(50, self.channel, 'mgmt')
    #     self.assertIsNone(tested_warning)
    #     tested_warning = warn(75, self.channel, 'mgmt')
    #     self.assertIsNone(tested_warning)
    #     tested_warning = warn(90, self.channel, 'mgmt')
    #     expected_warning = "*Please note*: work for pentest *{0}* has used
    #       more than {1}% of budgeted hours.".format(
    #         self.channel, 90)
    #     self.assertEqual(tested_warning, expected_warning)
    #     tested_warning = warn(100, self.channel, 'mgmt')
    #     expected_warning = "*WARNING*: work for pentest *{0}* has gone over
    #       budget.".format(
    #         self.channel)
    #     self.assertEqual(tested_warning, expected_warning)
    #     tested_warning = warn(50, self.channel, 'pentest')
    #     expected_warning = "We have used more than {0}% of budgeted hours
    #       for this pentest".format(
    #         50)
    #     self.assertEqual(tested_warning, expected_warning)
    #     tested_warning = warn(75, self.channel, 'pentest')
    #     self.assertIsNotNone(tested_warning)
    #     tested_warning = warn(90, self.channel, 'pentest')
    #     self.assertIsNotNone(tested_warning)
    #     tested_warning = warn(100, self.channel, 'pentest')
    #     self.assertIsNotNone(tested_warning)
    #     tested_warning = warn(0, self.channel, 'pentest')
    #     self.assertIsNone(tested_warning)
