import datetime
from unittest import TestCase

from ros.auth import User
from ros.charge import Charge
from ros.db import TemporaryDatabase
from ros.project import Project, ProjectType


class ChargeTest(TestCase):
    def test_new(self):
        user = User("Hans", "Numbers", "hans@example.com")
        project = Project(ProjectType.SALES, "Salo")
        date = datetime.date(1988, 9, 7)
        hours = 3
        charge = Charge(user, project, date, hours)
        self.assertEquals(charge.user, user)
        self.assertEquals(charge.project, project)
        self.assertEquals(charge.date, date)
        self.assertEquals(charge.hours, hours)

    def test_storage(self):
        user = User("Hans", "Numbers", "hans@example.com")
        project = Project(ProjectType.SALES, "Salo")
        date = datetime.date(1988, 9, 7)
        hours = 3
        description = "Digits make numbers"
        charge = Charge(user, project, date, hours)
        charge.description = description
        with TemporaryDatabase() as db:
            session = db.session
            session.add(charge)
            session.commit()
            charge = (
                session.query(Charge).filter(Charge.id == charge.id).first()
            )
            self.assertEquals(charge._user_id, user.id)
            self.assertEquals(charge.user, user)
            self.assertEquals(charge.project, project)
            self.assertEquals(charge.date, date)
            self.assertEquals(charge.hours, hours)
            self.assertEquals(charge.description, description)
