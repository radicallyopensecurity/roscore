from unittest import TestCase

import gitlab

from ros.gitlab import client


class GitLabTest(TestCase):
    def testClient(self):
        configuration = {
            'gitlab': {
                'url': 'https://example.com',
                'token': 'foo',
                'namespace': 'bar',
                'user': {
                    'name': 'ros',
                    'email': 'ros@example.com',
                },
            },
        }
        actual_client = client(configuration)
        assert isinstance(actual_client, gitlab.Gitlab)
        self.assertTrue(actual_client.api_url.startswith(configuration['gitlab']['url']))
