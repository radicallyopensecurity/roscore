from unittest import TestCase

from parameterized import parameterized

from ros.progress import ProgressBar


class ProgressBarTest(TestCase):
    @parameterized.expand([
        (100, 50,
         '[::::::::::::::::::::::::::::::::::::::::::::::::::..................................................] [50.0%]'),
        (100, 120,
         '[::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::] [120.0%]'),
    ])
    def test(self, max_value, current_value, expected):
        progress_bar = ProgressBar(max_value, current_value)
        self.assertEqual(str(progress_bar), expected)
